<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Uphist extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('records_model','',TRUE);
		if($this->session->userdata('logged_in'))
		{
			//set header links depending on logged in users in userdata session
			$this->header_links = $this->session->userdata('logged_in');

			$session_data = $this->session->userdata('logged_in');
			switch($session_data['usertype'])
			{
				// case 'sysad':
				// break;

				// default:
				// 	echo '<script>alert("You are not allowed to access this portion of the site!!!!");</script>';
				// 	redirect('','refresh');
			}
		}
		else
		{
			//If no session, redirect to login page
			redirect('../', 'refresh');
		}
	}
	function index() {
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');

			switch($session_data['usertype'])
			{
				case 'sysad':
					$data['files'] = $this->records_model->getAllRecords('uploads');
					foreach($data['files'] as $key => $value)
					{
						$data['files'][$key]['actions'] = anchor(base_url().'records/uphist/download/'.$value['id'], 'Download');
					}
					$loadedViews = array(
							'records/records_header_view' => $this->header_links,
							'records/uphist/uphist_view' => $data
							);
					$this->load->template($loadedViews, $this->header_links);
				break;

				default;
					echo '<script>alert("You are not allowed to access this portion of the site!");</script>';
					redirect('','refresh');
			}
		}
	}

	function download($id) {
		$filedata = $this->records_model->getRecordById('uploads', $id);
		$file = $filedata[0]['path'].$filedata[0]['hash'].'.'.end(explode(".", $filedata[0]['filename']));
		$mime = get_mime_by_extension($file);
		file_download($filedata[0]['filename'], $mime, $file);
	}

	function downloadTemp($id) {
		$filedata = $this->records_model->getRecordById('template', $id);
		$file = $filedata[0]['path'].$filedata[0]['hash'].'.'.end(explode(".", $filedata[0]['filename']));
		$mime = get_mime_by_extension($file);
		file_download($filedata[0]['filename'], $mime, $file);
	}

	function downloadAttachment($id) {
		$filedata = $this->records_model->getRecordById('attachments', $id);
		$file = $filedata[0]['path'].$filedata[0]['hash'].'.'.end(explode(".", $filedata[0]['filename']));
		$mime = get_mime_by_extension($file);

		$pdf = array('application/pdf',
				'application/x-pdf',
				'application/x-download',
				'binary/octet-stream',
				'application/unknown',
				'application/force-download');
		foreach($pdf as $key => $value)
		{
			if($mime == $value)
			{
				$file_pdf = TRUE;
			}
		}

		if($file_pdf == TRUE)
		{
			echo '<head><title>View '.$filename.'</title></head>';
			$this->output
	           ->set_content_type($mime)
	           ->set_output(file_get_contents($filedata[0]['path'].$filedata[0]['hash'].'.pdf'));
		}
		else
		{
			file_download($filedata[0]['filename'], $mime, $file);
		}
	}

	function view_pdf($uploaded_to,$filename)
	{
		$filename = urldecode($filename);
		$filedata = $this->records_model->getRecordByMultiField('uploads',
														array('uploaded_to'=>$uploaded_to,
															'filename'=>$filename));
		$file = $filedata[0]['path'].$filedata[0]['hash'].'.'.end(explode('.', $filedata[0]['filename']));
		$mime = get_mime_by_extension($file);
		echo '<head><title>View '.$filename.'</title></head>';
		$this->output
           ->set_content_type($mime)
           ->set_output(file_get_contents($filedata[0]['path'].$filedata[0]['hash'].'.pdf'));
	}

	function viewUploadedMembers($company_id,$file_id)
	{
		$company_details = $this->records_model->getRecordById('company_insurance',$company_id);
		$file_details = $this->records_model->getRecordById('uploads',$file_id);

		$members_uploaded = $this->records_model->getRecordByField('patient_company_insurance','uploaded_filename',$file_details[0]['filename']);

		foreach($members_uploaded as $key => $value)
		{
			$details = $this->records_model->getRecordByField('patient','special_id',$value['patient_special_id']);
			foreach($details as $dkey => $dvalue)
			{
				$members_details[$key] = $dvalue;
			}
		}
		$result[0]['company_insurance'] = $company_details[0];
		$result[0]['file_details'] = $file_details[0];
		$result[0]['members_details'] = $members_details;

		foreach($result as $key => $data)
		{
			$loadedViews = array(
                'records/records_header_view' => $this->header_links,
                'records/uphist/uploaded_company_members_view' => $data
            );
            $this->load->template($loadedViews, $this->header_links);
		}
	}

	function deleteUploadedRecords()
	{
		$data = $_POST;
		$count = 0;
		foreach($data['selMulti'] as $id)
		{
			$delete = $this->records_model->deleteByField('patient','special_id', $id);
			$delete2 = $this->records_model->deleteByField('patient_company_insurance','patient_special_id',$id);
			$count++;
		}

		if($delete)
		{
			if(isset($data['compins_id']))
			{
				$this->session->set_flashdata('result', 'Deleted '.$count.' patient(s)');
				$data['message'] = 'Deleted '.$count.' members.';
				$data['redirect'] = 'records/compins/members/'.$data['compins_id'];
				echo json_encode($data);
			}
			else
			{
				$this->session->set_flashdata('result', 'Deleted '.$count.' patient(s)');
				$data['message'] = 'Deleted '.$count.' members.';
				$data['redirect'] = 'records/members';
				echo json_encode($data);
			}
		}
	}
}
?>