<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Billing Accounts</title>
        <link href="<?php echo base_url();?>bootstrap/css/main-style.css" rel="stylesheet">
        <script>
            $(document).ready(function() {
                $(".selectAll").click(function() {
                    var checked_status = this.checked;
                    $(".selMulti").each(function() {
                        this.checked = checked_status;
                    });
                });
            });
        </script>
    </head>
        <body>
            <div class="profile_name">Accounts Billing List</div>
            <?php echo validation_errors(); ?>
            <?php echo form_open('records/accounts/verifyPassword'); ?>
            <?php
                    echo '<div class="table_scroll">';
                    $template = array(
                                'table_open'    => '<table border="1" cellpadding="4" cellspacing="0" class="table table-bordered table-hover">',
                                
                                'heading_row_start'   => '<tr>',
                                'heading_row_end'     => '</tr>',
                                'heading_cell_start'  => '<th>',
                                'heading_cell_end'    => '</th>',

                                'row_start'           => '<tr>',
                                'row_end'             => '</tr>',
                                'cell_start'          => '<td>',
                                'cell_end'            => '</td>',

                                'row_alt_start'       => '<tr>',
                                'row_alt_end'         => '</tr>',
                                'cell_alt_start'      => '<td>',
                                'cell_alt_end'        => '</td>',

                                'table_close'   => '</table>'
                                );
                    $this->table->set_template($template);
                    $this->table->set_heading(
                                                '',
                                                'Billing Request No.',
                                                'Reference No.',
                                                'Insurance',
                                                'Date Requested',
                                                'Prepared By',
                                                'Position',
                                                'Received By',
                                                'Position',
                                                'Billing Type',
                                                'Date Created'
                                            );
                    $count=1;

                    foreach ($billing_accounts_results as $key => $value)
                    {
                        if($value['type'] == 'Company')
                        {
                            $details_table[$key] = '<table class="table table-bordered" cellpadding="2" cellspacing="0" style="width:100%">
                                                        <tr>
                                                            <th>COMPANY</th>
                                                            <th>IP & OP</th>
                                                            <th>IP</th>
                                                            <th>ER</th>
                                                            <th>DENTAL</th>
                                                            <th>APE</th>
                                                            <th>DECLARATION DATE</th>
                                                            <th>RELEASE DATE</th>
                                                            <th>DATE COVERAGE</th>
                                                            <th>REMARKS</th>
                                                            <th>DATE CREATED</th>
                                                        </tr>
                                                    ';
                            foreach ($value['account_billing'] as $account_billing_key => $account_billing_value)
                            {
                                $details_table[$key] .= '
                                                        <tr>
                                                            <td>'.$account_billing_value['company'].'</td>
                                                            <td>'.$account_billing_value['ipop'].'</td>
                                                            <td>'.$account_billing_value['ip'].'</td>
                                                            <td>'.$account_billing_value['er'].'</td>
                                                            <td>'.$account_billing_value['dental'].'</td>
                                                            <td>'.$account_billing_value['ape'].'</td>
                                                            <td>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['declaration_date'])).'</td>
                                                            <td>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['release_date'])).'</td>
                                                            <td>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['effectivity_date'])).' - '.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['validity_date'])).'</td>
                                                            <td>'.$account_billing_value['remarks'].'</td>
                                                            <td>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['date_created'])).'</td>
                                                        </tr>
                                                ';
                            }
                            $details_table[$key].= '</table>';
                        }
                        else if($value['type'] == 'Members')
                        {
                            @$details_table[$key] .= '<table class="table table-bordered" cellpadding="2" cellspacing="0" style="width:100%">
                                                        <tr>
                                                            <th>COMPANY</th>
                                                            <th>MEMBERS</th>
                                                            <th>MEDICAL PLAN</th>
                                                            <th>AMOUNT</th>
                                                            <th>DECLARATION DATE</th>
                                                            <th>DATE COVERAGE</th>
                                                            <th>REMARKS</th>
                                                            <th>DATE CREATED</th>
                                                        </tr>
                                                    ';
                            foreach ($value['account_billing'] as $account_billing_key => $account_billing_value)
                            {
                                $details_table[$key] .= '
                                                        <tr>
                                                            <td>'.$account_billing_value['company'].'</td>
                                                            <td>'.$account_billing_value['members'].'</td>
                                                            <td>'.$account_billing_value['medical_plan'].'</td>
                                                            <td>'.$account_billing_value['amount'].'</td>
                                                            <td>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['declaration_date'])).'</td>
                                                            <td>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['effectivity_date'])).' - '.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['validity_date'])).'</td>
                                                            <td>'.$account_billing_value['remarks'].'</td>
                                                            <td>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['date_created'])).'</td>
                                                        </tr>
                                                ';
                            }
                            $details_table[$key].= '</table>';
                        }
                        
                        $selMulti = form_checkbox(array('name'=>'selMulti[]','id'=>'selMulti','class'=>'selMulti','value'=>$key[0]['id']));

                        if($value['type'] == 'Company')
                        {
                            if($value['attached_patient_list'] == '1')
                            {
                                $patient_list[$key] = 'With Patient List';
                            }
                            else
                            {
                                $patient_list[$key] = 'Without Patient List';
                            }

                            if($value['attached_billing_statement'] == '1')
                            {
                                $biling_statement[$key] = 'With Billing Statement';
                            }
                            else
                            {
                                $biling_statement[$key] = 'Without Biling Statement';
                            }

                            $type[$key] = $value['type'];
                            $type[$key].= '<br>'.$patient_list[$key];
                            $type[$key].= '<br>'.$biling_statement[$key];
                        }
                        else
                        {
                            $type[$key] = $value['type'];
                        }
                        
                        $this->table->add_row(
                                                $count++.".".$selMulti,
                                                anchor(
                                                        base_url()."records/accounts/view/accounts_billing/".$value['id']."/",
                                                        $value['billing_request_number']
                                                    ),
                                                // $value['billing_request_number'],
                                                $value['reference_number'],
                                                $value['insurance'],
                                                mdate('%M %d, %Y', mysql_to_unix($value['date_requested'])),
                                                $value['prepared_by'],
                                                $value['prepared_by_position'],
                                                $value['received_by'],
                                                $value['received_by_position'],
                                                $type[$key],
                                                // $details_table[$key],
                                                mdate('%M %d, %Y', mysql_to_unix($value['date_created']))
                                            );
                    }
                    echo $this->table->generate();
            ?>
        </div>
    </body>
</html>