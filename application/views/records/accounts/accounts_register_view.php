<script>
    $(document).ready(function () {
        $('#datestart, #dateend, #date_requested, #multiple_declaration, #multiple_release, #multiple_effectivity, #multiple_validity').datepicker({format: 'yyyy-mm-dd'});
        $('#formAddInsurance, #formAddBroker,#formAddCompany,#formAddCompanySingle,#formAddCompanyMultiple,#formAddMember,#formAddMemberSingle,#formAddMemberMultiple,#formAddBilling, #formAddBillingType, #formAddBillingCompany, #formAddBillingPatient').hide();
        $('#toggleSlideInsurance').click(function ()
        {
            $('#formAddInsurance').slideToggle('fast', function () {
            });
            $('#formAddBroker').hide();
            $('#formAddCompany').hide();
            $('#formAddMember').hide();
            $('#formAddBillingType').hide();
            $('#formAddBillingCompany').hide();
            $('#formAddBillingPatient').hide();
        });

        $('#toggleSlideBroker').click(function ()
        {
            $('#formAddBroker').slideToggle('fast', function () {
            });
            $('#formAddInsurance').hide();
            $('#formAddCompany').hide();
            $('#formAddMember').hide();
            $('#formAddBillingType').hide();
            $('#formAddBillingCompany').hide();
            $('#formAddBillingPatient').hide();
        });

        $('#toggleSlideCompany').click(function ()
        {
            $('#formAddCompany').slideToggle('fast', function () {
            });
            $('#formAddInsurance').hide();
            $('#formAddBroker').hide();
            $('#formAddMember').hide();
            $('#formAddBillingType').hide();
            $('#formAddBillingCompany').hide();
            $('#formAddBillingPatient').hide();
        });
        $('#toggleCompanySingle').click(function ()
        {
            $('#formAddCompanySingle').slideToggle('fast', function () {
            });
            $('#formAddCompanyMultiple').hide();
        });
        $('#toggleCompanyMultiple').click(function ()
        {
            $('#formAddCompanyMultiple').slideToggle('fast', function () {
            });
            $('#formAddCompanySingle').hide();
        });

        $('#toggleSlideMember').click(function ()
        {
            $('#formAddMember').slideToggle('fast', function () {
            });
            $('#formAddInsurance').hide();
            $('#formAddBroker').hide();
            $('#formAddCompany').hide();
            $('#formAddBillingType').hide();
            $('#formAddBillingCompany').hide();
            $('#formAddBillingPatient').hide();
        });
        $('#toggleMemberSingle').click(function ()
        {
            $('#formAddMemberSingle').slideToggle('fast', function () {
            });
            $('#formAddMemberMultiple').hide();
        });
        $('#toggleMemberMultiple').click(function ()
        {
            $('#formAddMemberMultiple').slideToggle('fast', function () {
            });
            $('#formAddMemberSingle').hide();
        });

        $('#toggleSlideBilling').click(function ()
        {
            $('#formAddBillingType').slideToggle('fast', function () {
            });
            $('#formAddCompany').hide();
            $('#formAddMember').hide();
            $('#formAddInsurance').hide();
            $('#formAddBroker').hide();
            $('#formAddBillingCompany').hide();
            $('#formAddBillingPatient').hide();
        });

        $('#billing_for').change(function () {
            var selected = $(this).val();
            if (selected == 'Company')
            {
                $('#formAddBillingCompany').slideToggle('fast', function () {
                });
                $('#formAddBillingPatient').hide();
                $('[name=type]').val(selected);
            }
            else if (selected == 'Members')
            {
                $('#formAddBillingPatient').slideToggle('fast', function () {
                });
                $('#formAddBillingCompany').hide();
                $('[name=type]').val(selected);
            }
            else
            {
                $('#formAddBillingCompany').hide();
                $('#formAddBillingPatient').hide();
            }
        });

        $('#compins-ins, #compins-insurance, #compins-ins_upload').autocomplete({
            minLength: 1,
            source: function (req, add) {
                $.ajax({
                    url: '<?= base_url() ?>utils/autocomplete/from/compins_insurance', //Controller where search is performed
                    dataType: 'json',
                    type: 'POST',
                    data: req,
                    success: function (data) {
                        if (data.response == 'true') {
                            add(data.message);
                        }
                    }
                });
            },
            select: function (event, ui)
            {
                $('[name=insurance_id]').val(ui.item ? ui.item.insurance_id : '');
            }
        });

        $('#compins-broker,#compins-broker_upload').autocomplete({
            minLength: 1,
            source: function (req, add) {
                $.ajax({
                    url: '<?= base_url() ?>utils/autocomplete/from/compins-brokers', //Controller where search is performed
                    dataType: 'json',
                    type: 'POST',
                    data: req,
                    success: function (data) {
                        if (data.response == 'true') {
                            add(data.message);
                        }
                    }
                });
            },
            select: function (event, ui)
            {
                $('[name=broker_id]').val(ui.item ? ui.item.broker_id : '');
            }
        });

        $("form#insurance_form").validate({
            rules: {
                name: {
                    required: true
                },
                attention_name: {
                    required: true
                },
                attention_position: {
                    required: true
                },
                address: {
                    required: true
                },
                street: {
                    required: true
                },
                city: {
                    required: true
                },
                state: {
                    required: true
                },
                country: {
                    required: true
                },
                postal_code: {
                    required: true
                },
                code: {
                    required: true
                },
                billing_code: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: 'This field is required'
                },
                attention_name: {
                    required: 'This field is required'
                },
                attention_position: {
                    required: 'This field is required'
                },
                address: {
                    required: 'This field is required'
                },
                street: {
                    required: 'This field is required'
                },
                city: {
                    required: 'This field is required'
                },
                state: {
                    required: 'This field is required'
                },
                country: {
                    required: 'This field is required'
                },
                postal_code: {
                    required: 'This field is required'
                },
                code: {
                    required: 'This field is required'
                },
                billing_code: {
                    required: 'This field is required'
                }
            }
        });

        $("form#broker_form").validate({
            rules: {
                name: {
                    required: true
                },
                address: {
                    required: true
                },
                street: {
                    required: true
                },
                city: {
                    required: true
                },
                state: {
                    required: true
                },
                country: {
                    required: true
                },
                postal_code: {
                    required: true
                },
                contact_person: {
                    required: true
                },
                contact_no: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: 'This field is required'
                },
                address: {
                    required: 'This field is required'
                },
                street: {
                    required: 'This field is required'
                },
                city: {
                    required: 'This field is required'
                },
                state: {
                    required: 'This field is required'
                },
                country: {
                    required: 'This field is required'
                },
                postal_code: {
                    required: 'This field is required'
                },
                contact_person: {
                    required: 'This field is required'
                },
                contact_no: {
                    required: 'This field is required'
                }
            }
        });

        $("form#company_form").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                insurance: {
                    required: true
                },
                start: {
                    required: true
                },
                end: {
                    required: true
                },
                'compins-ins':{
                    required: true
                }
            },
            messages: {
                name: {
                    required: 'This field is required'
                },
                code: {
                    required: "This field is required"
                }, insurance: {
                    required: "This field is required"
                },
                start: {
                    required: 'This field is required'
                },
                end: {
                    required: 'This field is required'
                },
                'compins-ins': {
                    required: 'This field is required'
                }
            }
        });

         $("form#company_form_upload").validate({
            rules: {
                'compins-ins_upload': {
                    required: true
                }
            },
            messages: {
                'compins-ins_upload': {
                    required: 'This field is required'
                }
            }
        });

        $("form#billing_company").validate({
            rules: {
                insurance: {
                    required: true
                },
                date_requested: {
                    required: true
                },
                billing_request_number: {
                    required: true
                },
                reference_number: {
                    required: true
                },
                prepared_by: {
                    required: true
                },
                prepared_by_position: {
                    required: true
                },
                received_by: {
                    required: true
                },
                received_by_position: {
                    required: true
                }
            },
            messages: {
                insurance: {
                    required: 'This field is required'
                },
                date_requested: {
                    required: 'This field is required'
                },
                billing_request_number: {
                    required: 'This field is required'
                },
                reference_number: {
                    required: 'This field is required'
                },
                prepared_by: {
                    required: 'This field is required'
                },
                prepared_by_position: {
                    required: 'This field is required'
                },
                received_by: {
                    required: 'This field is required'
                },
                received_by_position: {
                    required: 'This field is required'
                }
            }
        });

        $("form#billing_patient").validate({
            rules: {
                insurance: {
                    required: true
                },
                company: {
                    required: true
                },
                billing_attention_name: {
                    required: true
                },
                date_requested: {
                    required: true
                },
                billing_request_number: {
                    required: true
                },
                reference_number: {
                    required: true
                },
                prepared_by: {
                    required: true
                },
                prepared_by_position: {
                    required: true
                },
                received_by: {
                    required: true
                },
                received_by_position: {
                    required: true
                }
            },
            messages: {
                insurance: {
                    required: 'This field is required'
                },
                company: {
                    required: 'This field is required'
                },
                billing_attention_name: {
                    required: 'This field is required'
                },
                date_requested: {
                    required: 'This field is required'
                },
                billing_request_number: {
                    required: 'This field is required'
                },
                reference_number: {
                    required: 'This field is required'
                },
                prepared_by: {
                    required: 'This field is required'
                },
                prepared_by_position: {
                    required: 'This field is required'
                },
                received_by: {
                    required: 'This field is required'
                },
                received_by_position: {
                    required: 'This field is required'
                }
            }
        });

        $('#multiple_company, #company').autocomplete({
            minLength: 1,
            source: function (req, add) {
                $.ajax({
                    url: '<?= base_url() ?>utils/autocomplete/from/compins-comp', //Controller where search is performed
                    dataType: 'json',
                    type: 'POST',
                    data: req,
                    success: function (data) {
                        if (data.response == 'true') {
                            add(data.message);
                        }
                    }
                });
            },
            select: function (event, ui)
            {
                $('[name=company_id]').val(ui.item ? ui.item.company_id : '');
            }
        });

        var cloneCount = 1;
        $('#removeCompany').hide();
        $('#addCompany').click(function ()
        {
            $('#removeCompany').show();
            cloneCount++;
            $("#multiple_company_table").clone().appendTo('#multiple_company_fieldset').find("*[id]").andSelf().each(function ()
            {
                $(this).attr("id", $(this).attr("id") + cloneCount);
                $('#removeCompany').hide();
            });

            $('#multiple_declaration' + cloneCount).datepicker({format: 'yyyy-mm-dd'});
            $('#multiple_release' + cloneCount).datepicker({format: 'yyyy-mm-dd'});
            $('#multiple_effectivity' + cloneCount).datepicker({format: 'yyyy-mm-dd'});
            $('#multiple_validity' + cloneCount).datepicker({format: 'yyyy-mm-dd'});

            $('#removeCompany' + cloneCount).click(function ()
            {
                if (cloneCount > 1)
                {
                    $('#multiple_company_table' + cloneCount).remove();
                    cloneCount--;
                }
            });

            $('#multiple_company' + cloneCount).autocomplete(
                    {
                        minLength: 1,
                        source: function (req, add) {
                            $.ajax({
                                url: '<?= base_url() ?>utils/autocomplete/from/compins-comp', //Controller where search is performed
                                dataType: 'json',
                                type: 'POST',
                                data: req,
                                success: function (data) {
                                    if (data.response == 'true') {
                                        add(data.message);
                                    }
                                }
                            });
                        },
                    });
        });

        $('#billing_attention_name, #multiple_patient').autocomplete(
                {
                    minLength: 1,
                    source: function (req, add) {
                        $.ajax({
                            url: '<?= base_url() ?>utils/autocomplete/from/accounts-members', //Controller where search is performed
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success: function (data) {
                                if (data.response == 'true') {
                                    add(data.message);
                                }
                            }
                        });
                    },
                });

        $('#removePatient').hide();
        $('#addPatient').click(function ()
        {
            $('#removePatient').show();
            cloneCount++;
            $('#removePatient').show();
            cloneCount++;
            $("#multiple_patient_table").clone().appendTo('#multiple_patient_fieldset').find("*[id]").andSelf().each(function ()
            {
                $(this).attr("id", $(this).attr("id") + cloneCount);
                $('#removePatient').hide();
            });

            $('#multiple_declaration' + cloneCount).datepicker({format: 'yyyy-mm-dd'});
            $('#multiple_effectivity' + cloneCount).datepicker({format: 'yyyy-mm-dd'});
            $('#multiple_validity' + cloneCount).datepicker({format: 'yyyy-mm-dd'});

            $('#removePatient' + cloneCount).click(function ()
            {
                if (cloneCount > 1)
                {
                    $('#multiple_patient_table' + cloneCount).remove();
                    cloneCount--;
                }
            });

            $('#multiple_patient' + cloneCount).autocomplete(
                    {
                        minLength: 1,
                        source: function (req, add) {
                            $.ajax({
                                url: '<?= base_url() ?>utils/autocomplete/from/accounts-members', //Controller where search is performed
                                dataType: 'json',
                                type: 'POST',
                                data: req,
                                success: function (data) {
                                    if (data.response == 'true') {
                                        add(data.message);
                                    }
                                }
                            });
                        },
                    });
        });

        $('#dateofbirth').datepicker({format: 'yyyy-mm-dd'});
    $('#declaration_date').datepicker({format: 'yyyy-mm-dd'});
    $('#start').datepicker({format: 'yyyy-mm-dd'});
    $('#end').datepicker({format: 'yyyy-mm-dd'});

    $('#dependent_name').hide();
    $('input[name=cardholder_type]').click(function() {
        if($(this).val() == 'PRINCIPAL') {
                $('#dependent_name').hide();
        } else {
                $('#dependent_name').show();
        }
    });
    $('#dependent_name', this).autocomplete({
        minLength: 1,
        source: function(req, add){
            $.ajax({
                url: '<?=base_url()?>utils/autocomplete/from/cardholder', //Controller where search is performed
                dataType: 'json',
                type: 'POST',
                data: req,
                success: function(data) {
                    if(data.response =='true'){
                        add(data.message);
                    }
                }
            });
        }
    });
    $('#company_insurance,#company_insurance_multi').autocomplete({
        minLength: 1,
        source: function(req, add){
            $.ajax({
                url: '<?=base_url()?>utils/autocomplete/from/company_insurance', //Controller where search is performed
                dataType: 'json',
                type: 'POST',
                data: req,
                success: function(data) {
                    if(data.response =='true'){
                        add(data.message);
                    }
                }
            });
        },
        select: function(event, ui) {
                $('[name=company_insurance_id]').val(ui.item ? ui.item.compins_id : '');
        }
    });
    $("form#single_members").validate({
                rules: {
                            firstname: {
                                required: true
                            },
                    middlename: {
                        required: true
                    },
                    lastname: {
                        required: true
                    },
                    dateofbirth: {
                        required: true
                    },
                    level: {
                        required: true
                    },
                    declaration_date: {
                        required: true
                    },
                    start: {
                        required: true
                    },
                    end: {
                        required: true
                    },
                    company_insurance: {
                        required: true
                    },

                            philhealth: {
                        required: true
                    }
                },
                messages: {
                    firstname: {
                        required: 'This field is required'
                    },
                    middlename: {
                        required: 'This field is required'
                    },
                    lastname: {
                        required: 'This field is required'
                    },
                    dateofbirth: {
                        required: 'This field is required'
                    },
                    level: {
                        required: 'This field is required'
                    },
                    declaration_date: {
                        required: 'This field is required'
                    },
                    start: {
                        required: 'This field is required'
                    },
                    end: {
                        required: 'This field is required'
                    },
                    company_insurance: {
                        required: 'This field is required'
                    },

                            philhealth: {
                                required: 'This field is required'
                            }
                }
            });
            $("form#multiple_members").validate({
                rules: {
                    company_insurance_multi: {
                        required: true
                    }
                },
                messages: {
                    company_insurance_multi: {
                        required: 'This field is required'
                    }
                }
            });
    });
</script>

<h1>Accounts</h1>
<?php
if ($this->session->flashdata('result') != '') {
    echo '<b><font color="red">' . $this->session->flashdata('result') . "</font></b><br>";
}
?>
<img width="60px" src="<?php echo base_url();?>includes/images/ic_add.png" data-toggle="modal" data-target="#Add" id="addButton">

<div class="modal fade" id="Add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <button id='toggleSlideInsurance' class="btn btn-default">Insurance</button>
<button id='toggleSlideBroker' class="btn btn-default">Broker</button>
<button id='toggleSlideCompany' class="btn btn-default">Company</button>
<button id='toggleSlideMember' class="btn btn-default">Member</button>
<button id='toggleSlideBilling' class="btn btn-default">Billing</button>
                </div>
                <div class="modal-body">
<div id='formAddInsurance'>
    <h2>Insurance</h2>
    <?php echo validation_errors(); ?>
    <?php echo form_open_multipart('records/uphist/downloadTemp/1'); ?>
    <?php
    $inputs = array(
        array(form_label('Download Template for Insurance', 'multiup'), form_submit(array('value' => 'Download', 'class' => 'btn btn-warning')))
    );
    $template = array(
        'table_open' => '<table border="0" cellpadding="4" cellspacing="0">',
        'table_close' => '</table>'
    );
    $this->table->set_template($template);
    echo $this->table->generate($inputs);
    ?>
    <?php echo form_close(); ?>

    <?php echo validation_errors(); ?>
    <?php echo form_open_multipart('utils/fileuploader/upto/insurance'); ?>
    <?php
    $inputs = array(
        array(form_label('Upload multiple Insurance', 'multiup'), form_upload(array('name' => 'file', 'id' => 'multiup', 'class' => 'form-group')), form_submit(array('value' => 'Upload', 'class' => 'btn btn-success')))
    );
    $template = array(
        'table_open' => '<table border="0" cellpadding="4" cellspacing="0">',
        'table_close' => '</table>'
    );
    $this->table->set_template($template);
    echo $this->table->generate($inputs);
    ?>
    <?php echo form_close(); ?>

    <?php echo validation_errors(); ?>
    <?php echo form_open('records/accounts/register', array('id' => 'insurance_form')); ?>
    <?php
    $template = array(
        'table_open' => '<table border="0" cellpadding="4" cellspacing="0">',
        'table_close' => '</table>'
    );
    $this->table->set_template($template);
    $address_tmpl = array(
        array(form_label('Street', 'street'), form_input(array('name' => 'street', 'id' => 'street', 'size' => '20', 'placeholder' => 'Street', 'class' => 'form-control'))),
        array(form_label('City', 'city'), form_input(array('name' => 'city', 'id' => 'city', 'size' => '20', 'placeholder' => 'City', 'class' => 'form-control'))),
        array(form_label('State', 'state'), form_input(array('name' => 'state', 'id' => 'state', 'size' => '20', 'placeholder' => 'State', 'class' => 'form-control'))),
        array(form_label('Country'),form_input(array('name'=>'country','id'=>'country','size'=>'20','placeholder'=>'Country','class'=>'form-control'))),
        array(form_label('Postal Code', 'postal_code'), form_input(array('name' => 'postal_code', 'id' => 'postal_code', 'size' => '20', 'placeholder' => 'Postal Code', 'class' => 'form-control'))),
        );
    $address = form_fieldset('Address');
    $address.= $this->table->generate($address_tmpl);
    $address.= form_fieldset_close();
    $inputs = array(
        array('', ''),
        array(form_label('Insurance', 'name'), form_input(array('name' => 'name', 'id' => 'name', 'size' => '20', 'placeholder' => 'Insurance Name', 'class' => 'form-control'))),
        array(form_label('Attention Name', 'attention_name'), form_input(array('name' => 'attention_name', 'id' => 'attention_name', 'size' => '20', 'placeholder' => 'Attention Name', 'class' => 'form-control'))),
        array(form_label('Attention Position', 'attention_position'), form_input(array('name' => 'attention_position', 'id' => 'attention_position', 'size' => '20', 'placeholder' => 'Attention Position', 'class' => 'form-control'))),
        array(form_label('Address', 'address'), form_textarea(array('name' => 'address', 'id' => 'address', 'cols' => '20', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Complete Address'))),
        array('',$address),
        // array(form_label('Code', 'code'), form_input(array('name' => 'code', 'id' => 'code', 'size' => '20', 'placeholder' => 'Code', 'class' => 'form-control'))),
        // array(form_label('Vendor Account', 'vendor_account'), form_input(array('name'=>'vendor_account', 'id'=>'vendor_account', 'size'=>'20'))),
        // array(form_label('Billing Code', 'billing_code'), form_input(array('name' => 'billing_code', 'id' => 'billing_code', 'size' => '20', 'placeholder' => 'Billing Code', 'class' => 'form-control'))),
        array('', form_submit(array('name' => 'submit', 'value' => 'Register', 'class' => 'btn btn-success')))
    );
    $this->table->set_template($template);
    echo $this->table->generate($inputs);
    echo form_hidden('count', 0);
    echo form_hidden('count_Op', 0);
    echo form_hidden('table', 'insurance');
    ?>
    <?php echo form_close(); ?>
</div>

<div id='formAddBroker'>
    <h2>Broker</h2>
    <?php echo validation_errors(); ?>
    <?php echo form_open('records/accounts/register', array('id' => 'broker_form')); ?>
    <?php
    $template = array(
        'table_open' => '<table border="0" cellpadding="4" cellspacing="0">',
        'table_close' => '</table>'
    );

    $inputs = array(
        array('', ''),
        array(form_label('Broker', 'broker_name'), form_input(array('name' => 'name', 'id' => 'broker_name', 'size' => '50', 'class' => 'form-control', 'placeholder' => 'Name'))),
        array(form_label('Address', 'address'), form_textarea(array('name' => 'address', 'id' => 'address', 'cols' => '20', 'rows' => '3', 'class' => 'form-control', 'placeholder' => 'Complete Address'))),
        array(form_label('Street', 'street'), form_input(array('name' => 'street', 'id' => 'street', 'size' => '20', 'placeholder' => 'Street', 'class' => 'form-control'))),
        array(form_label('City', 'city'), form_input(array('name' => 'city', 'id' => 'city', 'size' => '20', 'placeholder' => 'City', 'class' => 'form-control'))),
        array(form_label('State', 'state'), form_input(array('name' => 'state', 'id' => 'state', 'size' => '20', 'placeholder' => 'State', 'class' => 'form-control'))),
        array(form_label('Country'),form_input(array('name'=>'country','id'=>'country','size'=>'20','placeholder'=>'Country','class'=>'form-control'))),
        array(form_label('Postal Code', 'postal_code'), form_input(array('name' => 'postal_code', 'id' => 'postal_code', 'size' => '20', 'placeholder' => 'Postal Code', 'class' => 'form-control'))),
        array(form_label('Contact Person', 'contact_person'), form_input(array('name' => 'contact_person', 'id' => 'contact_person', 'size' => '50', 'class' => 'form-control', 'placeholder' => 'Contact Person'))),
        array(form_label('Contact No.', 'contact_no'), form_input(array('name' => 'contact_no', 'id' => 'contact_no', 'size' => '50', 'class' => 'form-control', 'placeholder' => 'Contact No.'))),
        array('', form_submit(array('name' => 'submit', 'value' => 'Register', 'class' => 'btn btn-success')))
    );
    $this->table->set_template($template);
    echo $this->table->generate($inputs);
    echo form_hidden('table', 'brokers');
    ?>
    <?php echo form_close(); ?>
</div>

<div id='formAddCompany'>
    <h2>Company</h2>
    <button id='toggleCompanySingle' class="btn btn-default">Single</button>
    <button id='toggleCompanyMultiple' class="btn btn-default">Multiple</button>

    <div id='formAddCompanyMultiple'>
        <?php echo validation_errors(); ?>
        <?php echo form_open_multipart('records/uphist/downloadTemp/2'); ?>
        <?php
        $inputs = array(
            array(form_label('Download Template for Company', 'multiup'), form_submit(array('value' => 'Download', 'class' => 'btn btn-warning')))
        );
        $template = array(
            'table_open' => '<table border="0" cellpadding="4" cellspacing="0">',
            'table_close' => '</table>'
        );
        $this->table->set_template($template);
        echo $this->table->generate($inputs);
        ?>
        <?php echo form_close(); ?>

        <?php echo validation_errors(); ?>
        <?php echo form_open_multipart('utils/fileuploader/upto/company',array('id'=>'company_form_upload')); ?>
        <?php
        $inputs = array(
            array(form_label('Insurance'), form_input(array('name' => 'compins-ins_upload', 'id' => 'compins-ins_upload', 'size' => '50', 'class' => 'form-control', 'placeholder' => 'Insurance'))),
            array(form_label('Broker'), form_input(array('name' => 'broker_name', 'id' => 'compins-broker_upload', 'size' => '50', 'class' => 'form-control', 'placeholder' => 'Broker Name'))),
            array(form_label('Upload multiple Companies', 'multiup'), form_upload(array('name' => 'file', 'id' => 'multiup', 'class' => 'form-group')), form_submit(array('value' => 'Upload', 'class' => 'btn btn-success')))
        );
        $template = array(
            'table_open' => '<table border="0" cellpadding="4" cellspacing="0">',
            'table_close' => '</table>'
        );
        $this->table->set_template($template);
        echo $this->table->generate($inputs);
        echo form_hidden('insurance_id');
        echo form_hidden('broker_id');
        ?>
        <?php echo form_close(); ?>
    </div>

    <div id='formAddCompanySingle'>
        <?php echo validation_errors(); ?>
        <?php echo form_open('records/accounts/register', array('id' => 'company_form')); ?>
        <?php
        $template = array(
            'table_open' => '<table border="0" cellpadding="4" cellspacing="0">',
            'table_close' => '</table>'
        );

        // TAGGING OF INSURANCE
        $this->table->set_template($template);
        $insurance_tmpl = array(
            array('', ''),
            array(form_label('Insurance'), form_input(array('name' => 'insurance', 'id' => 'compins-ins', 'size' => '50', 'class' => 'form-control', 'placeholder' => 'Insurance'))),
            array(form_label('Broker'), form_input(array('name' => 'broker_name', 'id' => 'compins-broker', 'size' => '50', 'class' => 'form-control', 'placeholder' => 'Broker Name'))),
            // array(form_label('Effective Date'), form_input(array('name' => 'start', 'id' => 'datestart', 'placeholder' => 'YYYY-MM-DD', 'class' => 'form-control', 'size' => '20'))),
            // array(form_label('Validity Date'), form_input(array('name' => 'end', 'id' => 'dateend', 'placeholder' => 'YYYY-MM-DD', 'class' => 'form-control', 'size' => '20'))),
            array(form_label('Remarks'), form_textarea(array('name' => 'notes', 'id' => 'notes', 'cols' => '50', 'rows' => '10', 'class' => 'form-control'))),
        );
        $insurance = form_fieldset('Insurance');
        $insurance.= $this->table->generate($insurance_tmpl);
        $insurance.= form_fieldset_close();
        $inputs = array(
            array(form_label('Company', 'name'), form_input(array('name' => 'name', 'id' => 'name', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'Company'))),
            // array(form_label('Address', 'address'), form_input(array('name' => 'address', 'id' => 'address', 'size' => '20', 'placeholder' => 'Address', 'class' => 'form-control'))),
            // array(form_label('Street', 'street'), form_input(array('name' => 'street', 'id' => 'street', 'size' => '20', 'placeholder' => 'Street', 'class' => 'form-control'))),
            // array(form_label('City', 'city'), form_input(array('name' => 'city', 'id' => 'city', 'size' => '20', 'placeholder' => 'City', 'class' => 'form-control'))),
            // array(form_label('State', 'state'), form_input(array('name' => 'state', 'id' => 'state', 'size' => '20', 'placeholder' => 'State', 'class' => 'form-control'))),
            // array(form_label('Postal Code', 'postal_code'), form_input(array('name' => 'postal_code', 'id' => 'postal_code', 'size' => '20', 'placeholder' => 'Postal Code', 'class' => 'form-control'))),
            // array(form_label('Code', 'code'), form_input(array('name' => 'code', 'id' => 'code', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'Code'))),
            array(form_label(''), $insurance),
            array('', form_submit(array('name' => 'submit', 'value' => 'Register', 'class' => 'btn btn-success')))
        );
        $this->table->set_template($template);
        echo $this->table->generate($inputs);
        echo form_hidden('table', 'company');
        echo form_hidden('broker_id');
        echo form_hidden('insurance_id');
        ?>
        <?php echo form_close(); ?>
    </div>
</div>

<div id='formAddMember'>
    <h2>Members</h2>
    <button id='toggleMemberSingle' class="btn btn-default">Single</button>
    <button id='toggleMemberMultiple' class="btn btn-default">Multiple</button>

    <div id='formAddMemberMultiple'>
        <?php echo validation_errors(); ?>
        <?php echo form_open_multipart('records/uphist/downloadTemp/3');?>
            <?php
                $template = array(
                            'table_open'    => '<table border="0" cellpadding="4" cellspacing="0">',
                            'table_close'   => '</table>'
                            );
                $inputs = array(
                                array(form_label('Download Template for Members', 'multiup'), form_submit(array('value'=>'Download','class'=>'btn btn-warning')))
                                );
                echo $this->table->generate($inputs);
            ?>
        <?php echo form_close(); ?>

        <?php echo form_open_multipart('utils/fileuploader/upto/company_insurance_members', array('id' => 'multiple_members'));?>
            <?php
                $template = array(
                            'table_open'    => '<table border="0" cellpadding="4" cellspacing="0">',
                            'table_close'   => '</table>'
                            );
                $inputs = array(
                                array(form_label('Company'), form_input(array('name'=>'company_insurance_multi', 'id'=>'company_insurance_multi', 'size'=>'150','class'=>'form-control','placeholder'=>'Company'))),
                                array(form_label('Upload multiple Members', 'multicompany'), form_upload(array('name'=>'file', 'id'=>'multicompany','class'=>'form-group'))),
                                array('', form_submit(array('value'=>'Upload','class'=>'btn btn-success')))
                                );
                $this->table->set_template($template);
                echo $this->table->generate($inputs);
                echo form_hidden('company_insurance_id');
            ?>
        <?php echo form_close(); ?>
    </div>

    <div id='formAddMemberSingle'>
        <?php echo validation_errors(); ?>
        <?php echo form_open('records/accounts/register', array('id' => 'single_members')); ?>
            <?php
                $inputs = array(
                                array('', ''),
                                array(form_label('First Name', 'firstname'), form_input(array('name'=>'firstname', 'id'=>'firstname', 'size'=>'50','class'=>'form-control','placeholder'=>'First Name'))),
                                array(form_label('Middle Name', 'middlename'), form_input(array('name'=>'middlename', 'id'=>'middlename', 'size'=>'50','class'=>'form-control','placeholder'=>'Middle Name'))),
                                array(form_label('Last Name', 'lastname'), form_input(array('name'=>'lastname', 'id'=>'lastname', 'size'=>'50','class'=>'form-control','placeholder'=>'Last Name'))),
                                array(form_label('Date Of Birth', 'dateofbirth'), form_input(array('name'=>'dateofbirth', 'id'=>'dateofbirth', 'placeholder'=>'YYYY-MM-DD','class'=>'form-control'))),
                                array(form_label('Level/Position', 'level'), form_input(array('name'=>'level', 'id'=>'level', 'size'=>'50','class'=>'form-control','placeholder'=>'Level'))),
                                array(form_label('Status', 'status'), form_dropdown('status', array('ACTIVE' => 'ACTIVE', 'EXPIRED' => 'EXPIRED', 'DELETED' => 'DELETED', 'ON HOLD' => 'ON HOLD'))),
                                array(form_label('Date Of Declaration', 'declaration_date'), form_input(array('name'=>'declaration_date', 'id'=>'declaration_date', 'placeholder'=>'YYYY-MM-DD','class'=>'form-control','placeholder'=>'YYYY-MM-DD'))),
                                array(form_label('Effectivity Date'), form_input(array('name'=>'start', 'id'=>'start', 'placeholder'=>'YYYY-MM-DD','class'=>'form-control'))),
                                array(form_label('Validity Date'), form_input(array('name'=>'end', 'id'=>'end', 'placeholder'=>'YYYY-MM-DD','class'=>'form-control'))),
                                array(form_label('Remarks', 'remarks'), form_textarea(array('name'=>'remarks', 'id'=>'remarks', 'cols'=>'50', 'rows'=>'10','class'=>'form-control','placeholder'=>'Remarks'))),
                                array(form_label('Cardholder Type', 'cardholder_type'),
                                    form_radio(array('name'=>'cardholder_type', 'id'=>'cardholder_type', 'value'=>'PRINCIPAL'), '', TRUE).'Principal'.
                                    form_radio(array('name'=>'cardholder_type', 'id'=>'cardholder_type', 'value'=>'DEPENDENT'), '', FALSE).'Dependent'.'&nbsp'.
                                    form_input(array('name'=>'cardholder', 'id'=>'dependent_name', 'size'=>'50','class'=>'form-control','placeholder'=>'Dependent Name'))),
                                array(form_label('Company'), form_input(array('name'=>'company_insurance', 'id'=>'company_insurance', 'size'=>'150','class'=>'form-control','placeholder'=>'Company'))),
                                array(form_label('PhilHealth Benefit'),form_input(array('name'=>'philhealth','id'=>'philhealth','placeholder'=>'PhilHealth Benefit','size'=>'20','class'=>'form-control'))),
                                array(form_label('Pre-Existing Condition'),form_dropdown('pre_existing_condition',array(''=>'','Covered'=>'Covered','Not Covered'=>'Not Covered','Waived'=>'Waived','Subject for Approval' => 'Subject for Approval'))),
                                array('', form_submit(array('value'=>'Register','class'=>'btn btn-success')))
                                );
                $template = array(
                            'table_open'    => '<table border="0" cellpadding="4" cellspacing="0">',
                            'table_close'   => '</table>'
                            );
                $this->table->set_template($template);
                echo $this->table->generate($inputs);
                echo form_hidden('date_encoded', mdate('%Y-%m-%d', now()));
                echo form_hidden('company_insurance_id');
                echo form_hidden('table', 'patient');
            ?>
        <?php echo form_close(); ?>
    </div>
</div>

<div id="formAddBillingType">
    <h2>Billing</h2>
    <?php
    $tmpl = array(
        'table_open' => '<table border="0" cellpadding="4" cellspacing="0">',
        'table_close' => '</table>'
    );
    $type = array(
        array('', ''),
        array('Billing For: ', form_dropdown('billing_for', array('' => '', 'Company' => 'Company', 'Members' => 'Members'), '', 'id="billing_for"')),
    );
    $this->table->set_template($tmpl);
    echo $this->table->generate($type);
    ?>
</div>

<div id="formAddBillingCompany">
    <?php echo validation_errors(); ?>
    <?php echo form_open_multipart('records/accounts/printBilling', array('id'=>'billing_company')); ?>
    <?php
    $template = array(
        'table_open' => '<table border="0" cellpadding="4" cellspacing="0" id="multiple_company_table">',
        'table_close' => '</table>'
    );
    $this->table->set_template($template);

    $multipleCompany = array(
        array(form_label('Company Name'), form_input(array('name' => 'multiple_company[]', 'id' => 'multiple_company', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'Company Name'))),
        array(form_label('IP & OP'), form_input(array('name' => 'multiple_ipop[]', 'id' => 'multiple_ipop', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'IP & OP Count'))),
        array(form_label('IP'), form_input(array('name' => 'multiple_ip[]', 'id' => 'multiple_ip', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'IP Count'))),
        array(form_label('ER'), form_input(array('name' => 'multiple_er[]', 'id' => 'multiple_er', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'ER Count'))),
        array(form_label('Dental'), form_input(array('name' => 'multiple_dental[]', 'id' => 'multiple_dental', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'Dental Count'))),
        array(form_label('APE'), form_input(array('name' => 'multiple_ape[]', 'id' => 'multiple_ape', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'APE Count'))),
        array(form_label('Replacement'), form_input(array('name' => 'multiple_replacement[]', 'id' => 'multiple_replacement', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'Replacement Count'))),
        array(form_label('OP Only'), form_input(array('name' => 'multiple_op[]', 'id' => 'multiple_op', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'OP Count'))),
        array(form_label('Date of Declaration'), form_input(array('name' => 'multiple_declaration[]', 'id' => 'multiple_declaration', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'YYYY-MM-DD'))),
        array(form_label('Date of Release'), form_input(array('name' => 'multiple_release[]', 'id' => 'multiple_release', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'YYYY-MM-DD'))),
        array(form_label('Coverage Date'), ''), // COVERAGE PERIOD (start and end FIELD IN patient TABLE, effectivity_date and validity_date HERE);
        array(form_label('Effectivity Date'), form_input(array('name' => 'multiple_effectivity[]', 'id' => 'multiple_effectivity', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'YYYY-MM-DD'))),
        array(form_label('Validity Date'), form_input(array('name' => 'multiple_validity[]', 'id' => 'multiple_validity', 'class' => 'form-control', 'placeholder' => 'YYYY-MM-DD', 'size' => '20'))),
        array(form_label('Remarks'), form_textarea(array('name' => 'multiple_remarks[]', 'id' => 'multiple_remarks', 'cols' => '30', 'rows' => '5', 'class' => 'form-control', 'placeholder' => 'Remarks'))),
        array(form_button(array('name' => 'removeCompany[]', 'id' => 'removeCompany', 'content' => 'Remove Company', 'class' => 'btn btn-xs btn-danger')), ''),
        array('', '')
    );
    $addMultipleCompany = form_fieldset('Company', array('id' => 'multiple_company_fieldset'));
    $addMultipleCompany.= $this->table->generate($multipleCompany);
    $addMultipleCompany . form_fieldset_close();

    $tmpl = array(
        'table_open' => '<table border="0" cellpadding="4" cellspacing="0">',
        'table_close' => '</table>'
    );

    $type = array(
        array('', ''),
        array('Billing For: ', form_dropdown('type', array('' => '', 'Company' => 'Company', 'Members' => 'Members')))
    );
    $inputs = array(
        array('', ''),
        array(form_label('Insurance'), form_input(array('name' => 'insurance', 'id' => 'compins-insurance', 'size' => '50', 'class' => 'form-control', 'placeholder' => 'Insurance'))),
        array(form_button(array('name' => 'addCompany', 'id' => 'addCompany', 'content' => 'Add Company', 'class' => 'btn btn-info btn-xs')), $addMultipleCompany),
        array(form_label('Date Requested'), form_input(array('name' => 'date_requested', 'id' => 'date_requested', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'YYYY-MM-DD'))),
        array(form_label('Billing Request Number'), form_input(array('name' => 'billing_request_number', 'id' => 'billing_request_number', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'Billing Request Number'))),
        array(form_label('Reference Number'), form_input(array('name' => 'reference_number', 'id' => 'reference_number', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'Reference Number'))),
        array(form_label('Prepared By'), form_input(array('name' => 'prepared_by', 'id' => 'prepared_by', 'size' => '20', 'placeholder' => 'Prepared By', 'class' => 'form-control'))),
        array(form_label('Position'), form_input(array('name' => 'prepared_by_position', 'id' => 'prepared_position', 'size' => '20', 'placeholder' => 'Prepared By Position', 'class' => 'form-control'))),
        array(form_label('Received By'), form_input(array('name' => 'received_by', 'id' => 'received_by', 'size' => '20', 'placeholder' => 'Received By', 'class' => 'form-control'))),
        array(form_label('Position'), form_input(array('name' => 'received_by_position', 'id' => 'received_position', 'size' => '20', 'placeholder' => 'Received By Position', 'class' => 'form-control'))),
        array(form_label('Members List'), form_upload(array('name' => 'file', 'id' => 'file', 'class' => 'form-group'))),
        array('', form_submit(array('name' => 'submit', 'id' => 'submit', 'class' => 'btn btn-success', 'value' => 'Save')))
    );
    $this->table->set_template($tmpl);
    echo $this->table->generate($inputs);
    echo form_hidden('insurance_id');
    echo form_hidden('type');
    ?>
    <?php echo form_close(); ?>
</div>

<div id="formAddBillingPatient">
    <?php echo validation_errors(); ?>
    <?php echo form_open('records/accounts/printBilling', array('id'=>'billing_patient')); ?>
    <?php
    $template = array(
        'table_open' => '<table border="0" cellpadding="4" cellspacing="0" id="multiple_patient_table">',
        'table_close' => '</table>'
    );
    $this->table->set_template($template);

    $multiplePatient = array(
        array(form_label('Patient Name'), form_input(array('name' => 'multiple_patient[]', 'id' => 'multiple_patient', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'Patient Name'))),
        array(form_label('Medical Plan'), form_input(array('name' => 'multiple_medical_plan[]', 'id' => 'multiple_medical_plan', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'Medical Plan'))),
        array(form_label('Amount'), form_input(array('name' => 'multiple_amount[]', 'id' => 'multiple_amount', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'Amount'))),
        array(form_label('Date of Declaration'), form_input(array('name' => 'multiple_declaration[]', 'id' => 'multiple_declaration', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'YYYY-MM-DD'))),
        array(form_label('Coverage Date'), ''), // COVERAGE PERIOD (start and end FIELD IN patient TABLE, effectivity_date and validity_date HERE);
        array(form_label('Effectivity Date'), form_input(array('name' => 'multiple_effectivity[]', 'id' => 'multiple_effectivity', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'YYYY-MM-DD'))),
        array(form_label('Validity Date'), form_input(array('name' => 'multiple_validity[]', 'id' => 'multiple_validity', 'class' => 'form-control', 'placeholder' => 'YYYY-MM-DD', 'size' => '20'))),
        array(form_label('Remarks'), form_textarea(array('name' => 'multiple_remarks[]', 'id' => 'multiple_remarks', 'cols' => '30', 'rows' => '5', 'class' => 'form-control', 'placeholder' => 'Remarks'))),
        array(form_button(array('name' => 'removePatient[]', 'id' => 'removePatient', 'content' => 'Remove Patient', 'class' => 'btn btn-xs btn-danger')), ''),
        array('', '')
    );
    $addMultiplePatient = form_fieldset('Patient', array('id' => 'multiple_patient_fieldset'));
    $addMultiplePatient.= $this->table->generate($multiplePatient);
    $addMultiplePatient . form_fieldset_close();

    $tmpl = array(
        'table_open' => '<table border="0" cellpadding="4" cellspacing="0">',
        'table_close' => '</table>'
    );
    $inputs = array(
        array('', ''),
        array(form_label('Insurance'), form_input(array('name' => 'insurance', 'id' => 'compins-insurance', 'size' => '50', 'class' => 'form-control', 'placeholder' => 'Insurance'))),
        array(form_label('Company Name'), form_input(array('name' => 'company', 'id' => 'company', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'Company Name'))),
        array(form_label('Attention To'), form_input(array('name' => 'billing_attention_name', 'id' => 'billing_attention_name', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'Attention to'))),
        array(form_button(array('name' => 'addPatient', 'id' => 'addPatient', 'content' => 'Add Patient', 'class' => 'btn btn-info btn-xs')), $addMultiplePatient),
        array(form_label('Date Requested'), form_input(array('name' => 'date_requested', 'id' => 'date_requested', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'YYYY-MM-DD'))),
        array(form_label('Billing Request Number'), form_input(array('name' => 'billing_request_number', 'id' => 'billing_request_number', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'Billing Request Number'))),
        array(form_label('Reference Number'), form_input(array('name' => 'reference_number', 'id' => 'reference_number', 'size' => '20', 'class' => 'form-control', 'placeholder' => 'Reference Number'))),
        array(form_label('Prepared By'), form_input(array('name' => 'prepared_by', 'id' => 'prepared_by', 'size' => '20', 'placeholder' => 'Prepared By', 'class' => 'form-control'))),
        array(form_label('Position'), form_input(array('name' => 'prepared_by_position', 'id' => 'prepared_position', 'size' => '20', 'placeholder' => 'Prepared By Position', 'class' => 'form-control'))),
        array(form_label('Received By'), form_input(array('name' => 'received_by', 'id' => 'received_by', 'size' => '20', 'placeholder' => 'Received By', 'class' => 'form-control'))),
        array(form_label('Position'), form_input(array('name' => 'received_by_position', 'id' => 'received_position', 'size' => '20', 'placeholder' => 'Received By Position', 'class' => 'form-control'))),
        array('', form_submit(array('name' => 'submit', 'id' => 'submit', 'class' => 'btn btn-success', 'value' => 'Save')))
    );
    $this->table->set_template($tmpl);
    echo $this->table->generate($inputs);
    echo form_hidden('insurance_id');
    echo form_hidden('company_id');
    echo form_hidden('type');
    ?>
    <?php echo form_close(); ?>

    </div>
            </div>
          </div>
        </div>
</div>