<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Billing Accounts</title>
        <link href="<?php echo base_url();?>bootstrap/css/main-style.css" rel="stylesheet">
        <script>
        $(document).ready(function() {
            $('.editable').editable('<?=base_url()?>utils/ajaxeditinplace', {
                indicator : 'Saving...',
                cancel    : 'Cancel',
                submit    : 'OK',
                tooltip   : 'Click to edit...',
                onblur    : 'cancel',
                width     : '100%',
                submitdata : {table: 'accounts_billing', key: <?=$id?>}
            });

            $('.editable2').editable('<?=base_url()?>utils/ajaxeditinplace', {
                type      : 'datepicker',
                datepicker: {
                    format: 'yyyy-mm-dd'
                },
                indicator : 'Saving...',
                cancel    : 'Cancel',
                submit    : 'OK',
                tooltip   : 'Click to edit...',
                onblur    : 'cancel',
                placeholder: 'YYYY-MM-DD',
                width     : '100%',
                submitdata : {table: 'accounts_billing', key: <?=$id?>},
                callback  : function(value, settings) {
                    window.location.reload(true);
                }
            });

            $('.editable3').on('mouseenter', function() {
                key = $(this).attr('data-key');
                id = $(this).attr('data-id');
                $(this).editable('<?=base_url()?>utils/ajaxeditinplace', {
                indicator : 'Saving...',
                cancel    : 'Cancel',
                submit    : 'OK',
                tooltip   : 'Click to edit...',
                onblur    : 'cancel',
                submitdata : {table: 'accounts_billing_company', key: key, id: id}
              });
            });

            $('.editable4').on('mouseenter', function() {
                key = $(this).attr('data-key');
                id = $(this).attr('data-id');
                $(this).editable('<?=base_url()?>utils/ajaxeditinplace', {
                datepicker: {
                    format: 'yyyy-mm-dd'
                },
                type      : 'datepicker',
                indicator : 'Saving...',
                cancel    : 'Cancel',
                submit    : 'OK',
                tooltip   : 'Click to edit...',
                onblur    : 'cancel',
                placeholder: 'YYYY-MM-DD',
                width     : '100%',
                submitdata : {table: 'accounts_billing_company', key: key, id: id}
              });
            });

            $('.editable5').on('mouseenter', function() {
                key = $(this).attr('data-key');
                id = $(this).attr('data-id');
                $(this).editable('<?=base_url()?>utils/ajaxeditinplace', {
                indicator : 'Saving...',
                cancel    : 'Cancel',
                submit    : 'OK',
                tooltip   : 'Click to edit...',
                onblur    : 'cancel',
                submitdata : {table: 'accounts_billing_members', key: key, id: id}
              });
            });

            $('.editable6').on('mouseenter', function() {
                key = $(this).attr('data-key');
                id = $(this).attr('data-id');
                $(this).editable('<?=base_url()?>utils/ajaxeditinplace', {
                datepicker: {
                    format: 'yyyy-mm-dd'
                },
                type      : 'datepicker',
                indicator : 'Saving...',
                cancel    : 'Cancel',
                submit    : 'OK',
                tooltip   : 'Click to edit...',
                onblur    : 'cancel',
                placeholder: 'YYYY-MM-DD',
                width     : '100%',
                submitdata : {table: 'accounts_billing_members', key: key, id: id}
              });
            });
        });
        </script>
    </head>
        <body>
            <div class="profile_container">
                <?php
                if ($this->session->flashdata('result') != '')
                {
                    echo '<b>'.$this->session->flashdata('result')."</b><br>";
                }
                ?>
                <div class="profile_name">Request for billing:  <b><?php echo $billing_request_number?></b></div>
                <?php

                    // PATIENT LIST ATTACHMENT
                    if(isset($attachment_patient_list))
                    {
                        $patient_list = array(
                            array($attachment_patient_list[0]['filename'],form_open_multipart('records/uphist/downloadAttachment/'.$attachment_patient_list[0]['id']).form_submit(array('value'=>'Download','class'=>'btn btn-warning')).form_close())
                            );
                    }
                    else
                    {
                        $patient_list = array(
                            array(form_open_multipart('utils/fileuploader/upload_attachment/accounts_billing_company_patient_list/accounts_billing/'.$id).form_upload(array('name'=>'file','id'=>'file','class'=>'form-group')).form_submit(array('value'=>'Upload','class'=>'btn btn-success')).form_close())
                            );
                    }

                    //BILLING STATEMENT ATTACHMENT
                    if(isset($attachment_billing_statement))
                    {
                        $billing_statement = array(
                            array($attachment_billing_statement[0]['filename'],form_open_multipart('records/uphist/downloadAttachment/'.$attachment_billing_statement[0]['id']).form_submit(array('value'=>'Download','class'=>'btn btn-warning')).form_close())
                            );
                    }
                    else
                    {
                        $billing_statement = array(
                            array(form_open_multipart('utils/fileuploader/upload_attachment/accounts_billing_company_billing_statement/accounts_billing/'.$id).form_upload(array('name'=>'file','id'=>'file','class'=>'form-group')).form_submit(array('value'=>'Upload','class'=>'btn btn-success')).form_close())
                            );
                    }

                    $template = array(
                        'table_open'    => '<table border="0" cellpadding="4" cellspacing="0">',
                        'table_close'   => '</table>'
                        );
                    $this->table->set_template($template);
                    $patient_list_function = $this->table->generate($patient_list);
                    $billing_statement_function = $this->table->generate($billing_statement);

                    $tmpl = array (
                                    'table_open'          => '<table cellpadding="4" cellspacing="0" class="profile_table">',
                                    'table_close'         => '</table>'
                                    );
                    $this->table->set_template($tmpl);
                    $back = anchor(base_url()."records/company/", "Back");

                    $this->table->add_row('Billing Request No.', '<div class="editable" id="billing_request_number">'
                        .$billing_request_number.
                        '</div>');
                    $this->table->add_row('Reference No.', '<div class="editable" id="reference_number">'
                        .$reference_number.
                        '</div>');
                    $this->table->add_row('Insurance', '<div class="editable" id="name">'
                        .$insurance.
                        '</div>');
                    $this->table->add_row('Date Requested', '<div class="editable2" id="date_requested">'
                        .mdate('%M %d, %Y', mysql_to_unix($date_requested)).
                        '</div>');
                    $this->table->add_row('Prepared By', '<div class="editable" id="prepared_by">'
                        .$prepared_by.
                        '</div>');
                    $this->table->add_row('Position', '<div class="editable" id="prepared_by_position">'
                        .$prepared_by_position.
                        '</div>');
                    $this->table->add_row('Received By', '<div class="editable" id="billing_request_number">'
                        .$received_by.
                        '</div>');
                    $this->table->add_row('Position', '<div class="editable" id="received_by">'
                        .$received_by_position.
                        '</div>');
                    // $this->table->add_row('Remarks', '<div class="editable" id="received_by_position">'
                    //     .$remarks.
                    //     '</div>');
                    $this->table->add_row('Date Created', '<div class="editable2" id="date_created">'
                        .mdate('%M %d, %Y', mysql_to_unix($date_created)).
                        '</div>');

                    if($type == 'Company')
                    {
                         $this->table->add_row('Members List',$patient_list_function);
                        $this->table->add_row('Billing Statement',$billing_statement_function);
                    }

                    $this->table->add_row(
                                            '',
                                            anchor(base_url()."records/accounts/reprintBilling/".$id, "Print", array('name' => 'submit', 'id' => 'submit', 'class' => 'btn btn-success', 'value' => 'Submit', 'target' => '_blank'))
                                        );

                    // $this->table->add_row(anchor(base_url()."records/company/delete/".$id."/", "Delete Company", array('onClick'=>"return confirm('Are you sure you want to delete this record?')",'class'=>'btn btn-danger')),'');

                    echo $this->table->generate();
                ?>
            </div>

            <div class="profile_name">Accounts Billing List</div>
            <?php echo validation_errors(); ?>
            <?php echo form_open('records/accounts/verifyPassword'); ?>
            <?php
                    $template = array(
                                'table_open' => '<table border="1" cellpadding="4" cellspacing="0" class="table table-bordered table-hover">',

                                'heading_row_start'   => '<tr>',
                                'heading_row_end'     => '</tr>',
                                'heading_cell_start'  => '<th>',
                                'heading_cell_end'    => '</th>',

                                'row_start'           => '<tr>',
                                'row_end'             => '</tr>',
                                'cell_start'          => '<td>',
                                'cell_end'            => '</td>',

                                'row_alt_start'       => '<tr>',
                                'row_alt_end'         => '</tr>',
                                'cell_alt_start'      => '<td>',
                                'cell_alt_end'        => '</td>',

                                'table_close'   => '</table>'
                                );
                    $this->table->set_template($template);
                    if($type == 'Company')
                    {
                        $this->table->set_heading(
                                                    'COMPANY',
                                                    'IP & OP',
                                                    'IP',
                                                    'ER',
                                                    'DENTAL',
                                                    'APE',
                                                    'REPLACEMENT',
                                                    'OP ONLY',
                                                    'DECLARATION DATE',
                                                    'RELEASE DATE',
                                                    'DATE COVERAGE',
                                                    'REMARKS',
                                                    'DATE CREATED'
                                                );
                        foreach ($account_billing_type as $account_billing_key => $account_billing_value)
                        {
                            $accounts_billing_id = $account_billing_value['id'];
                            $this->table->add_row(
                                                    $account_billing_value['company'],
                                                    '<div class="editable3" data-id="ipop" data-key='.$accounts_billing_id.'>'.$account_billing_value['ipop'].'</div>',
                                                    '<div class="editable3" data-id="ip" data-key='.$accounts_billing_id.'>'.$account_billing_value['ip'].'</div>',
                                                    '<div class="editable3" data-id="er" data-key='.$accounts_billing_id.'>'.$account_billing_value['er'].'</div>',
                                                    '<div class="editable3" data-id="dental" data-key='.$accounts_billing_id.'>'.$account_billing_value['dental'].'</div>',
                                                    '<div class="editable3" data-id="ape" data-key='.$accounts_billing_id.'>'.$account_billing_value['ape'].'</div>',
                                                    '<div class="editable3" data-id="replacement" data-key='.$accounts_billing_id.'>'.$account_billing_value['replacement'].'</div>',
                                                    '<div class="editable3" data-id="op" data-key='.$accounts_billing_id.'>'.$account_billing_value['op'].'</div>',
                                                    '<div class="editable4" data-id="declaration_date" data-key='.$accounts_billing_id.'>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['declaration_date'])).'</div>',
                                                    '<div class="editable4" data-id="release_date" data-key='.$accounts_billing_id.'>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['release_date'])).'</div>',
                                                    '<table>
                                                        <tr>
                                                            <td class="editable4" data-id="effectivity_date" data-key='.$accounts_billing_id.'>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['effectivity_date'])).'</td>
                                                            <td>-</td>
                                                            <td class="editable4" data-id="validity_date" data-key='.$accounts_billing_id.'>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['validity_date'])).'</td>
                                                        </tr>
                                                    </table>',
                                                    '<div class="editable3" data-id="remarks" data-key='.$accounts_billing_id.'>'.$account_billing_value['remarks'].'</div>',
                                                    '<div class="editable4" data-id="date_created" data-key='.$accounts_billing_id.'>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['date_created'])).'</div>'
                                                );
                        }
                    }
                    else if($type == 'Members')
                    {
                        $this->table->set_heading(
                                                    'COMPANY',
                                                    'MEMBERS',
                                                    'MEDICAL PLAN',
                                                    'AMOUNT',
                                                    'DECLARATION DATE',
                                                    'DATE COVERAGE',
                                                    'REMARKS',
                                                    'DATE CREATED'
                                                );
                        foreach ($account_billing_type as $account_billing_key => $account_billing_value)
                        {
                            $accounts_billing_id = $account_billing_value['id'];
                            $this->table->add_row(
                                                    $account_billing_value['company'],
                                                    $account_billing_value['members'],
                                                    // $account_billing_value['medical_plan'],
                                                    '<div class="editable5" data-id="medical_plan" data-key='.$accounts_billing_id.'>'.$account_billing_value['medical_plan'].'</div>',
                                                    // $account_billing_value['amount'],
                                                    '<div class="editable5" data-id="amount" data-key='.$accounts_billing_id.'>'.$account_billing_value['amount'].'</div>',
                                                    // mdate('%M %d, %Y', mysql_to_unix($account_billing_value['declaration_date'])),
                                                    '<div class="editable6" data-id="declaration_date" data-key='.$accounts_billing_id.'>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['declaration_date'])).'</div>',

                                                    // mdate('%M %d, %Y', mysql_to_unix($account_billing_value['effectivity_date'])).' - '.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['validity_date'])),

                                                    // '<div class="editable6" data-id="effectivity_date" data-key='.$accounts_billing_id.'>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['effectivity_date'])).'</div>'
                                                    // . '-' .
                                                    // '<div class="editable6" data-id="validity_date" data-key='.$accounts_billing_id.'>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['validity_date'])).'</div>',

                                                    '<table>
                                                        <tr>
                                                            <td class="editable6" data-id="effectivity_date" data-key='.$accounts_billing_id.'>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['effectivity_date'])).'</td>
                                                            <td>-</td>
                                                            <td class="editable6" data-id="validity_date" data-key='.$accounts_billing_id.'>'.mdate('%M %d, %Y', mysql_to_unix($account_billing_value['validity_date'])).'</td>
                                                        </tr>
                                                    </table>',

                                                    // $account_billing_value['remarks'],
                                                    '<div class="editable5" data-id="remarks" data-key='.$accounts_billing_id.'>'.$account_billing_value['remarks'].'</div>',
                                                    mdate('%M %d, %Y', mysql_to_unix($account_billing_value['date_created']))
                                                );
                        }
                    }
                    echo $this->table->generate();
            ?>
        </div>
    </body>
</html>