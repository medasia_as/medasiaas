<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Billing Accounts</title>
        <link href="<?php echo base_url();?>bootstrap/css/main-style.css" rel="stylesheet">
        <script>
        $(document).ready(function() {
            $('.editable').editable('<?=base_url()?>utils/ajaxeditinplace', {
                indicator : 'Saving...',
                cancel    : 'Cancel',
                submit    : 'OK',
                tooltip   : 'Click to edit...',
                onblur    : 'cancel',
                width     : '100%',
                submitdata : {table: 'accounts_billing', key: <?=$id?>}
            });

            $('.editable2').editable('<?=base_url()?>utils/ajaxeditinplace', {
                type      : 'datepicker',
                // datepicker: {
                //     format: 'yyyy-mm-dd'
                // },
                indicator : 'Saving...',
                cancel    : 'Cancel',
                submit    : 'OK',
                tooltip   : 'Click to edit...',
                onblur    : 'cancel',
                placeholder: 'YYYY-MM-DD',
                width     : '100%',
                submitdata : {table: 'accounts_billing', key: <?=$id?>},
                callback  : function(value, settings) {
                    window.location.reload(true);
                }
            });
        });
        </script>
    </head>
    <h1>*****accounts_billing_details_view*****</h1>
        <body>
            <div class="profile_container">
                <?php 
                if ($this->session->flashdata('result') != '')
                {
                    echo '<b>'.$this->session->flashdata('result')."</b><br>";
                }
                ?>
                <div class="profile_name">Request for billing:  <b><?php echo $billing_request_number?></b></div>
                <?php

                    // PATIENT LIST ATTACHMENT
                    if(isset($attachment_patient_list))
                    {
                        $patient_list = array(
                            array($attachment_patient_list[0]['filename'],form_open_multipart('records/uphist/downloadAttachment/'.$attachment_patient_list[0]['id']).form_submit(array('value'=>'Download','class'=>'btn btn-warning')).form_close())
                            );
                    }
                    else
                    {
                        $patient_list = array(
                            array(form_open_multipart('utils/fileuploader/upload_attachment/accounts_billing_company_patient_list/accounts_billing/'.$id).form_upload(array('name'=>'file','id'=>'file','class'=>'form-group')).form_submit(array('value'=>'Upload','class'=>'btn btn-success')).form_close())
                            );
                    }

                    //BILLING STATEMENT ATTACHMENT
                    if(isset($attachment_billing_statement))
                    {
                        $billing_statement = array(
                            array($attachment_billing_statement[0]['filename'],form_open_multipart('records/uphist/downloadAttachment/'.$attachment_billing_statement[0]['id']).form_submit(array('value'=>'Download','class'=>'btn btn-warning')).form_close())
                            );
                    }
                    else
                    {
                        $billing_statement = array(
                            array(form_open_multipart('utils/fileuploader/upload_attachment/accounts_billing_company_billing_statement/accounts_billing/'.$id).form_upload(array('name'=>'file','id'=>'file','class'=>'form-group')).form_submit(array('value'=>'Upload','class'=>'btn btn-success')).form_close())
                            );
                    }

                    $template = array(
                        'table_open'    => '<table border="0" cellpadding="4" cellspacing="0">',
                        'table_close'   => '</table>'
                        );
                    $this->table->set_template($template); 
                    $patient_list_function = $this->table->generate($patient_list);
                    $billing_statement_function = $this->table->generate($billing_statement);

                    $tmpl = array (
                                    'table_open'          => '<table cellpadding="4" cellspacing="0" class="profile_table">',
                                    'table_close'         => '</table>'
                                    );
                    $this->table->set_template($tmpl);
                    $back = anchor(base_url()."records/company/", "Back");

                    $this->table->add_row('Billing Request No.', '<div class="editable" id="billing_request_number">'
                        .$billing_request_number.
                        '</div>');
                    $this->table->add_row('Reference No.', '<div class="editable" id="reference_number">'
                        .$reference_number.
                        '</div>');
                    $this->table->add_row('Insurance', '<div class="editable" id="name">'
                        .$insurance.
                        '</div>');
                    $this->table->add_row('Date Requested', '<div class="editable2" id="date_requested">'
                        .mdate('%M %d, %Y', mysql_to_unix($date_requested)).
                        '</div>');
                    $this->table->add_row('Prepared By', '<div class="editable" id="prepared_by">'
                        .$prepared_by.
                        '</div>');
                    $this->table->add_row('Position', '<div class="editable" id="prepared_by_position">'
                        .$prepared_by_position.
                        '</div>');
                    $this->table->add_row('Received By', '<div class="editable" id="billing_request_number">'
                        .$billing_request_number.
                        '</div>');
                    $this->table->add_row('Position', '<div class="editable" id="received_by">'
                        .$received_by.
                        '</div>');
                    $this->table->add_row('Details', '<div class="editable" id="received_by_position">'
                        .$received_by_position.
                        '</div>');
                    $this->table->add_row('Date Created', '<div class="editable2" id="date_created">'
                        .mdate('%M %d, %Y', mysql_to_unix($date_created)).
                        '</div>');

                    if($type == 'Company')
                    {
                         $this->table->add_row('Members List',$patient_list_function);
                        $this->table->add_row('Billing Statement',$billing_statement_function);
                    }
                    
                    $this->table->add_row(
                                            '',
                                            anchor(base_url()."records/accounts/reprintBilling/".$id, "Reprint", array('name' => 'submit', 'id' => 'submit', 'class' => 'btn btn-success', 'value' => 'Submit'))
                                        );

                    // $this->table->add_row(anchor(base_url()."records/company/delete/".$id."/", "Delete Company", array('onClick'=>"return confirm('Are you sure you want to delete this record?')",'class'=>'btn btn-danger')),'');

                    echo $this->table->generate();
                ?>
            </div>

            <div class="profile_name">Accounts Billing List</div>
            <?php echo validation_errors(); ?>
            <?php echo form_open('records/accounts/verifyPassword'); ?>
            <?php
                    $template = array(
                                'table_open' => '<table border="1" cellpadding="4" cellspacing="0" class="table table-bordered table-hover">',
                                
                                'heading_row_start'   => '<tr>',
                                'heading_row_end'     => '</tr>',
                                'heading_cell_start'  => '<th>',
                                'heading_cell_end'    => '</th>',

                                'row_start'           => '<tr>',
                                'row_end'             => '</tr>',
                                'cell_start'          => '<td>',
                                'cell_end'            => '</td>',

                                'row_alt_start'       => '<tr>',
                                'row_alt_end'         => '</tr>',
                                'cell_alt_start'      => '<td>',
                                'cell_alt_end'        => '</td>',

                                'table_close'   => '</table>'
                                );
                    $this->table->set_template($template);
                    if($type == 'Company')
                    {
                        $this->table->set_heading(
                                                    'COMPANY',
                                                    'IP & OP',
                                                    'IP',
                                                    'ER',
                                                    'DENTAL',
                                                    'APE',
                                                    'REPLACEMENT',
                                                    'OP ONLY',
                                                    'DECLARATION DATE',
                                                    'RELEASE DATE',
                                                    'DATE COVERAGE',
                                                    'REMARKS',
                                                    'DATE CREATED'
                                                );
                        foreach ($account_billing_type as $account_billing_key => $account_billing_value)
                        {
                            $this->table->add_row(
                                                    $account_billing_value['company'],
                                                    $account_billing_value['ipop'],
                                                    $account_billing_value['ip'],
                                                    $account_billing_value['er'],
                                                    $account_billing_value['dental'],
                                                    $account_billing_value['ape'],
                                                    $account_billing_value['replacement'],
                                                    $account_billing_value['op'],
                                                    mdate('%M %d, %Y', mysql_to_unix($account_billing_value['declaration_date'])),
                                                    mdate('%M %d, %Y', mysql_to_unix($account_billing_value['release_date'])),
                                                    mdate('%M %d, %Y', mysql_to_unix($account_billing_value['effectivity_date'])).' - '
                                                             .mdate('%M %d, %Y', mysql_to_unix($account_billing_value['validity_date'])),
                                                            $account_billing_value['remarks'],
                                                    mdate('%M %d, %Y', mysql_to_unix($account_billing_value['date_created']))
                                                );
                        }
                    }
                    else if($type == 'Members')
                    {
                        $this->table->set_heading(
                                                    'COMPANY',
                                                    'MEMBERS',
                                                    'MEDICAL PLAN',
                                                    'AMOUNT',
                                                    'DECLARATION DATE',
                                                    'DATE COVERAGE',
                                                    'REMARKS',
                                                    'DATE CREATED'
                                                );
                        foreach ($account_billing_type as $account_billing_key => $account_billing_value)
                        {
                            $this->table->add_row(
                                                    $account_billing_value['company'],
                                                    $account_billing_value['members'],
                                                    $account_billing_value['medical_plan'],
                                                    $account_billing_value['amount'],
                                                    mdate('%M %d, %Y', mysql_to_unix($account_billing_value['declaration_date'])),
                                                    mdate('%M %d, %Y', mysql_to_unix($account_billing_value['effectivity_date'])).' - '
                                                             .mdate('%M %d, %Y', mysql_to_unix($account_billing_value['validity_date'])),
                                                    $account_billing_value['remarks'],
                                                    mdate('%M %d, %Y', mysql_to_unix($account_billing_value['date_created']))
                                                );
                        }
                    }
                    echo $this->table->generate();
            ?>
        </div>
    </body>
</html>