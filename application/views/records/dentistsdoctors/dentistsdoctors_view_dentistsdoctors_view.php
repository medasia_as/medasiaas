<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Medriks - Record of Doctor <?php echo $lastname.', '.$firstname.' '.$middlename;?></title>
		<script>
			$(document).ready(function() {
				$('.editable#type').editable('<?=base_url()?>utils/ajaxeditinplace', {
					loadurl   : '<?=base_url()?>utils/ajaxeditinplace/dentistsdoctors_type/<?=$type?>',
					type      : 'select',
					indicator : 'Saving...',
					cancel    : 'Cancel',
					submit    : 'OK',
					tooltip   : 'Click to edit...',
					onblur    : 'cancel',
					submitdata : {table: 'dentistsanddoctors', key: <?=$id?>}
				});

				$('.editable').editable('<?=base_url()?>utils/ajaxeditinplace', {
					indicator : 'Saving...',
					cancel    : 'Cancel',
					submit    : 'OK',
					tooltip   : 'Click to edit...',
					onblur    : 'cancel',
					submitdata : {table: 'dentistsanddoctors', key: <?=$id?>}
				});

				$('.editable2').editable('<?=base_url()?>utils/ajaxeditinplace', {
					type      : 'datepicker',
					datepicker: {
						format: 'yyyy-mm-dd'
					},
					indicator : 'Saving...',
					cancel    : 'Cancel',
					submit    : 'OK',
					tooltip   : 'Click to edit...',
					onblur    : 'cancel',
					placeholder : 'YYYY-MM-DD',
					submitdata : {table: 'dentistsanddoctors', key: <?=$id?>},
				});

				$('.editable3').editable('<?=base_url()?>utils/ajaxeditinplace', {
					loadurl   : '<?=base_url()?>utils/ajaxeditinplace/accred/<?=$type?>',
					type      : 'select',
					indicator : 'Saving...',
					cancel    : 'Cancel',
					submit    : 'OK',
					tooltip   : 'Click to edit...',
					onblur    : 'cancel',
					submitdata : {table: 'dentistsanddoctors', key: <?=$id?>}
				});

				$('.editable4').editable('<?=base_url()?>utils/ajaxeditinplace', {
					loadurl   : '<?=base_url()?>utils/ajaxeditinplace/doctor_status/<?=$doctor_status?>',
					type      : 'select',
					indicator : 'Saving...',
					cancel    : 'Cancel',
					submit    : 'OK',
					tooltip   : 'Click to edit...',
					onblur    : 'cancel',
					submitdata : {table: 'dentistsanddoctors', key: <?=$id?>}
				});

				$('.editableClinic').editable('<?=base_url()?>utils/ajaxeditinplace', {
					indicator : 'Saving...',
					cancel    : 'Cancel',
					submit    : 'OK',
					tooltip   : 'Click to edit...',
					onblur    : 'cancel',
					submitdata : function(value, settings) {
						var column_name = this.id.replace(/\[[0-9]+\]/g, "");
						var str = this.id;
						var pos = str.indexOf("[") + 1;
						var primary_key = str.slice(pos, -1);
						return {table: 'clinics', key: primary_key, id: column_name};
					}
				});

				$('#addclinic').click(function() {
					// alert($(this).attr);
					$('#clinic_infoTMPL').clone().appendTo('#clin');
				});

				$('#btnAddClinic').click(function(){
				    $("#add-clinic-form")[0].reset();
				});

				$('.edit_clinic').click(function(){
					var id = $(this).data('id');
					var name = $(this).data('name');
					var address = $(this).data('address');
					var phone_number = $(this).data('phone_number');
					var mobile_number = $(this).data('mobile_number');
					var street = $(this).data('street');
					var city = $(this).data('city');
					var state = $(this).data('state');
					var postal_code = $(this).data('postal_code');
					var country = $(this).data('country');

					$("input[name='establishment[id]']").val(id);
					$("input[name='establishment[name]']").val(name);
					$("input[name='establishment[address]']").val(address);
					$("input[name='establishment[phone_number]']").val(phone_number);
					$("input[name='establishment[mobile_number]']").val(mobile_number);
					$("input[name='establishment[street]']").val(street);
					$("input[name='establishment[city]']").val(city);
					$("input[name='establishment[state]']").val(state);
					$("input[name='establishment[postal_code]']").val(postal_code);
					$("input[name='establishment[country]']").val(country);
				});

				$('#add-clinic-form,#edit-clinic-form').bootstrapValidator({
					message: 'This value is not valid',
					fields: {
						"establishment[name]": {
							message: 'The Name is not valid',
							validators: {
								notEmpty: {
									message: 'Name is required and cannot be empty'
								}
							}
						},
						"establishment[address]": {
							message: 'Address is not valid',
							validators: {
								notEmpty: {
									message: 'Address is required and cannot be empty'
								}
							}
						},
						"establishment[street]": {
							message: 'Address is not valid',
							validators: {
								notEmpty: {
									message: 'street is required and cannot be empty'
								}
							}
						},
						"establishment[city]": {
							message: 'City is not valid',
							validators: {
								notEmpty: {
									message: 'City is required and cannot be empty'
								}
							}
						},
						"establishment[state]": {
							message: 'State is not valid',
							validators: {
								notEmpty: {
									message: 'State is required and cannot be empty'
								}
							}
						},
						"establishment[country]": {
							message: 'Country is not valid',
							validators: {
								notEmpty: {
									message: 'Country is required and cannot be empty'
								}
							}
						},
						"establishment[postal_code]": {
							message: 'Postal Code is not valid',
							validators: {
								regexp: {
									regexp: /^[0-9]+$/,
									message: 'Postal Code is not valid'
								}
							}
						},
					}
				});
			});
		</script>
	</head>
	<body>
		<h2>View record of <?php echo $lastname.', '.$firstname.' '.$middlename; ?></h2>
		<?php
		if($this->session->flashdata('result') != '')
		{
			echo '<b><font color="red">'.$this->session->flashdata('result')."</font></b><br>";
		}
		?>
		<?php
			//NEW CLINIC TEMPLATE
			$clinicTmpl = array(
					array('', ''),
					array(form_label('Clinic name', 'clinic_name'), form_input(array('name'=>'clinic_name[]', 'id'=>'clinic_name', 'size'=>'20'))),
					array(form_label('Hospital name', 'hospital_name'), form_input(array('name'=>'hospital_name[]', 'id'=>'hospital_name', 'size'=>'20'))),
					array(form_label('Street Address', 'street_address'), form_input(array('name'=>'street_address[]', 'id'=>'street_address', 'size'=>'20'))),
					array(form_label('Subdivision/Village', 'subdivision_village'), form_input(array('name'=>'subdivision_village[]', 'id'=>'subdivision_village', 'size'=>'20'))),
					array(form_label('Barangay', 'barangay'), form_input(array('name'=>'barangay[]', 'id'=>'barangay', 'size'=>'20'))),
					array(form_label('City', 'city'), form_input(array('name'=>'city[]', 'id'=>'city', 'size'=>'20'))),
					array(form_label('Province', 'province'), form_input(array('name'=>'province[]', 'id'=>'province', 'size'=>'20'))),
					array(form_label('Clinic Sched', 'clinic_sched'), form_input(array('name'=>'clinic_sched[]', 'id'=>'clinic_sched', 'size'=>'20'))),
					array(form_button('delete[]', 'Delete'),form_submit('add[]', 'Add')),
					array('', '')
				);
			$template = array(
						'table_open'	=> '<table border="0" cellpadding="4" cellspacing="0" id="clinicset">',
						'table_close'	=> '</table>'
					);
			$this->table->set_template($template);
			$clinic = form_open('records/dentistsdoctors/addClinic', array('id' => 'addClinicForm'));
			$clinic.= form_fieldset('Clinic Information', array('id'=>'clinic_infoTMPL'));
			$clinic.= $this->table->generate($clinicTmpl);
			$clinic.= form_fieldset_close();
			$clinic.= form_close();
			echo '<div style="display:none">'.$clinic.'</div>';

			// if($clinics != 0) {
			// 	$clinicForm = '';
			// 	foreach($clinics as $key => $value) {
			// 		$clinicTmpl = $this->table->add_row('Clinic name', '<div class="editableClinic" id="clinic_name['.$value['id'].']">'.$value['clinic_name'].'</div>');
			// 		$clinicTmpl = $this->table->add_row('Hospital name', '<div class="editableClinic" id="hospital_name['.$value['id'].']">'.$value['hospital_name'].'</div>');
			// 		$clinicTmpl = $this->table->add_row('Street address', '<div class="editableClinic" id="street_address['.$value['id'].']">'.$value['street_address'].'</div>');
			// 		$clinicTmpl = $this->table->add_row('Subdivision/Village', '<div class="editableClinic" id="subdivision_village['.$value['id'].']">'.$value['subdivision_village'].'</div>');
			// 		$clinicTmpl = $this->table->add_row('Barangay', '<div class="editableClinic" id="barangay['.$value['id'].']">'.$value['barangay'].'</div>');
			// 		$clinicTmpl = $this->table->add_row('City', '<div class="editableClinic" id="city['.$value['id'].']">'.$value['city'].'</div>');
			// 		$clinicTmpl = $this->table->add_row('Province', '<div class="editableClinic" id="province['.$value['id'].']">'.$value['province'].'</div>');
			// 		$clinicTmpl = $this->table->add_row('Region', '<div class="editableClinic" id="region['.$value['id'].']">'.$value['region'].'</div>');
			// 		$clinicTmpl = $this->table->add_row('Clinic Sched', '<div class="editableClinic" id="clinic_sched['.$value['id'].']">'.$value['clinic_sched'].'</div>');
			// 		$template = array(
			// 					'table_open'	=> '<table border="1" cellpadding="4" cellspacing="0" id="clinicset['.$value['id'].']">',
			// 					'table_close'	=> '</table>'
			// 					);
			// 		$this->table->set_template($template);
			// 		$clinicForm.= form_fieldset('Clinic Information', array('id'=>'clinic_info'));
			// 		$clinicForm.= $this->table->generate($clinicTmpl);
			// 		$clinicForm.= form_fieldset_close();
			// 	}
			// } else {
			// 	$clinicForm = 'NO CLINICS';

			// }

			// $clinicTmpl2 = $this->table->add_row('Address', '<div class="editable" id="clinic1">'.$clinic1.'</div>');
			// $clinicTmpl2 = $this->table->add_row('Address', '<div class="editable" id="clinic2">'.$clinic2.'</div>');
			// $clinicTmpl2 = $this->table->add_row('Address', '<div class="editable" id="clinic3">'.$clinic3.'</div>');
			// $clinicTmpl2 = $this->table->add_row('Address', '<div class="editable" id="clinic4">'.$clinic4.'</div>');
			// $clinicTmpl2 = $this->table->add_row('Address', '<div class="editable" id="clinic5">'.$clinic5.'</div>');

			// $template = array(
			// 				'table_open' => '<table border="1" cellpadding="4" cellspacing="0">',
			// 				'table_close' => '</table>'
			// 				);
			// $this->table->set_template($template);
			// $clinicForm2 = form_fieldset('<b>Clinic Information</b>', array('id'=>'clinic_info'));
			// $clinicForm2.= $this->table->generate($clinicTmpl2);
			// $clinicForm2.= form_fieldset_close();

			/* NEW CLINICS TABLE */
		  	if($clinics) {
			  	$tmpl = array(
			  			'table_open'          => '<table border="1" cellpadding="4" cellspacing="0" class="table table-bordered table-hover">',
			            'heading_row_start'   => '<tr>',
			            'heading_row_end'     => '</tr>',
			            'heading_cell_start'  => '<th>',
			            'heading_cell_end'    => '</th>',
			            'row_start'           => '<tr>',
			            'row_end'             => '</tr>',
		                'cell_start'          => '<td>',
		                'cell_end'            => '</td>',
		                'row_alt_start'       => '<tr>',
		                'row_alt_end'         => '</tr>',
		                'cell_alt_start'      => '<td>',
		                'cell_alt_end'        => '</td>',
		                'table_close'         => '</table>'
			  		);

			  $this->table->set_template($tmpl);
			  $new_clinic = $this->table->set_heading('', 'Name', 'Contact #', 'Address');
			  $count=1;

			  foreach ($clinics as $key => $value) {

			      if($value['phone_number']) {
			        if($value['mobile_number']) {
			          $contact_info = 'Phone: ' . $value['phone_number'] . ' / ' . 'Mobile: ' . $value['mobile_number'];
			        } else {
			          $contact_info = 'Phone: ' . $value['phone_number'];
			        }
			      } else if($value['mobile_number']) {
			        $contact_info = 'Mobile: ' . $value['mobile_number'];
			      } else {
			        $contact_info = 'N/A';
			      }

			      $edit_click = '<div class="edit_clinic" data-toggle="modal"
			                                            data-target="#editClinic"
			                                            data-id="'.$value['id'].'"
			                                            data-name="'.$value['name'].'"
			                                            data-phone_number="'.$value['phone_number'].'"
			                                            data-mobile_number="'.$value['mobile_number'].'"
			                                            data-address="'.$value['address'].'"
			                                            data-street="'.$value['street'].'"
			                                            data-city="'.$value['city'].'"
			                                            data-state="'.$value['state'].'"
			                                            data-postal_code="'.$value['postal_code'].'"
			                                            data-country="'.$value['country'].'">';

			      $new_clinic .= $this->table->add_row(
			                  $count++.".",
			                  $edit_click.$value['name'].'</div>',
			                  // $value['phone_number'],
			                  // $value['mobile_number'],
			                                            $edit_click.$contact_info.'</div>',
			                  $edit_click.$value['address'].'</div>'
			              );
			  }
			  $new_clinics = $this->table->generate($new_clinic);
			} else {
				$new_clinics = 'N/A';
			}
			/* END */

			/* FILE UPLOAD */
			$template = array(
					'table_open'  => '<table border="0" cellpadding="4" cellspacing="0">',
			        'table_close' => '</table>'
				);
			$this->table->set_template($template);
			$inputs = '<b>Upload Multiple Clinics</b>';
			$inputs.= form_open_multipart('utils/fileuploader/upto/establishments');
			$multiple_clinic = array(
					array(form_upload(array('name'=>'file', 'id'=>'multiup','class'=>'form-group')),form_submit(array('value'=>'Upload','class'=>'btn btn-success')))
				);
			$inputs.= $this->table->generate($multiple_clinic);
			$inputs.= form_hidden('doctor_id', $id);
			$inputs.= form_close();
			/* END */

			/* DOWNLOAD TEMPLATE */
			$download = form_open_multipart('records/uphist/downloadTemp/6');
			$download_button = array(
										array(form_label('Download Template for Clinics'), form_submit(array('value'=>'Download','class'=>'btn btn-warning')))
										);
			$download.= $this->table->generate($download_button);
			$download.= form_close();
			/* END */

			/* NEW CLINCS FUNCTIONS */
			$tmpl = array(
					'table_open'  => '<table border="0" cellpadding="4" cellspacing="0">',
			        'table_close' => '</table>'
				);
			$this->table->set_template($tmpl);
			// $new_clinic = $this->table->set_heading('', 'Name','Address');
			$functions = $this->table->add_row('New Clinics');
			$functions.= $this->table->add_row('<button type="button" id="btnAddClinic" class="btn btn-success" data-toggle="modal" data-target="#addClinic">Add More Clinic</button>');
			$functions.= $this->table->add_row($download);
			$functions.= $this->table->add_row($inputs);
			$new_clinics_functions = $this->table->generate($functions);
			/* END */

			$tmpl = array(
					'table_open'          => '<table border="1" cellpadding="4" cellspacing="0">',
					'table_close'         => '</table>'
				);
			$this->table->set_template($tmpl);
			$this->table->add_row('Doctor Status in Directory', '<div class="editable4" id="doctor_status">'.$doctor_status.'</div>');
			$this->table->add_row('Type', '<div class="editable" id="type">'.$type.'</div>');
			$this->table->add_row('Lastname', '<div class="editable" id="lastname">'.$lastname.'</div>');
			$this->table->add_row('Firstname', '<div class="editable" id="firstname">'.$firstname.'</div>');
			$this->table->add_row('Middlename', '<div class="editable" id="middlename">'.$middlename.'</div>');
			$this->table->add_row('Specialization', '<div class="editable" id="specialization">'.$specialization.'</div>');

			// if($clinics != 0) { $this->table->add_row('Clinic/s: ', '<div id="clin">'.$clinicForm.'</div>');
			// 	// $this->table->add_row(form_button(array('name' => 'addclinic', 'id' => 'addclinics', 'content' => 'Add clinic')), '<div id="clin">'.$clinicForm.'</div>');
			// } else {
			// 	$this->table->add_row('ADD/REMOVE CLINIC', $clinicForm);
			// }

			// $this->table->add_row('Clinic/s', $clinicForm2);

			$this->table->add_row('Mobile #', '<div class="editable" id="mobile_number">'.$mobile_number.'</div>');
			$this->table->add_row('Contact #', '<div class="editable" id="contact_number">'.$contact_number.'</div>');
			$this->table->add_row('Fax #', '<div class="editable" id="fax_number">'.$fax_number.'</div>');
			$this->table->add_row('E-mail Address','<div class="editable" id="email">'.$email.'</div>');
			$this->table->add_row('Date Accredited', '<div class="editable2" id="date_accredited">'.mdate('%M %d, %Y', mysql_to_unix($date_accredited)).'</div>');
			$this->table->add_row('Status', '<div class="editable3" id="status">'.$status.'</div>');
			$this->table->add_row('Remarks', '<div class="editable" id="remarks">'.$remarks.'</div>');
			$this->table->add_row($new_clinics_functions, $new_clinics);

			// echo anchor('delete/something', 'Delete', array('onClick' => "return confirm('Are you sure you want to delete?')"));

			// $this->table->add_row(anchor(base_url()."records/dentistsdoctors/delete/".$id."/", "Delete Dentist/Doctor", array('onClick'=>"return confirm('Are you sure you want to delete this record?')")), ''); //form_submit('submit', 'Update', 'onclick="addClinicForm.submit();"'));

			echo $this->table->generate();
		?>

		<br>
		<div>
		  <?php
		    if ($clinic1 || $clinic2 || $clinic3 || $clinic4 || $clinic5)
		    {
		      $tmpl1 = array(
			        'table_open'  => '<table border="0" cellpadding="4" cellspacing="0">',
			        'table_close' => '</table>'
		        );
		      $old = array(
			        array(''),
			        array($clinic1),
			        array($clinic2),
			        array($clinic3),
			        array($clinic4),
			        array($clinic5)
		        );
		      $this->table->set_template($tmpl1);
		      $old_clinic = form_fieldset('Old Clinics');
		      $old_clinic.= $this->table->generate($old);
		      $old_clinic.= form_fieldset_close();
		      $tmpl2 = array(
			        'table_open'  => '<table border="0" cellpadding="4" cellspacing="0">',
			        'table_close' => '</table>'
		        );
		      $this->table->set_template($tmpl2);
		      $this->table->add_row($old_clinic);
		      echo $this->table->generate();
		    }
		  ?>
		</div>

		<!-- Add Clinic Modal -->
		<div class="modal fade" id="addClinic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <form method="post" id="add-clinic-form" action="<?php echo site_url('records/dentistsdoctors/addClinic');?>" role="form">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		          <h4 class="modal-title" id="myModalLabel">Add Clinic</h4>
		        </div>
		        <div class="modal-body">
		          <div class="form-group">
		            <!-- <select type="text" id="clinicCategory" class="form-control" placeholder="Clinic Name" name="establishment[category]">
		              <option value="Medical Clinic">Medical Clinic</option>
		              <option value="Dental Clinic">Dental Clinic</option>
		            </select> -->
		          </div>
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="Clinic Name" name="establishment[name]">
		          </div>
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="Address" name="establishment[address]">
		          </div>
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="Phone No." name="establishment[phone_number]">
		          </div>

		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="Mobile No." name="establishment[mobile_number]">
		          </div>
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="Street" name="establishment[street]">
		          </div>
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="City" name="establishment[city]">
		          </div>
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="State" name="establishment[state]">
		          </div>
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="Country" name="establishment[country]">
		          </div>
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="Postal Code" name="establishment[postal_code]">
		          </div>
		          <?php echo form_hidden('establishment[doctor_id]',$id);?>
		          <div class="form-group" style="margin-bottom: 0">
		            <div class="bfh-selectbox bfh-countries" data-country="PH" data-name="establishment[country]" data-flags="true"></div>
		          </div>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		          <button type="submit" class="btn btn-primary">Save</button>
		        </div>
		      </form>
		    </div>
		  </div>
		</div>
		<!-- End of Modal -->

		<!-- Edit Clinic Modal -->
		<div class="modal fade" id="editClinic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <form method="post" id="edit-clinic-form" action="<?php echo site_url('records/dentistsdoctors/editClinic');?>" role="form">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		          <h4 class="modal-title" id="myModalLabel">Edit Clinic</h4>
		        </div>
		        <div class="modal-body">
		        <!--
		          <div class="form-group">
		            <select type="text" id="clinicCategory" class="form-control" placeholder="Clinic Name" name="establishment[category]">
		              <option value="Medical Clinic">Medical Clinic</option>
		              <option value="Dental Clinic">Dental Clinic</option>
		            </select>
		          </div>
		        -->
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="Clinic Name" name="establishment[name]">
		          </div>
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="Address" name="establishment[address]">
		          </div>
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="Phone No." name="establishment[phone_number]">
		          </div>
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="Mobile No." name="establishment[mobile_number]">
		          </div>
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="Street" name="establishment[street]">
		          </div>
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="City" name="establishment[city]">
		          </div>
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="State" name="establishment[state]">
		          </div>
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="Country" name="establishment[country]">
		          </div>
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="Postal Code" name="establishment[postal_code]">
		          </div>
		          <?php echo form_hidden('establishment[id]');?>
		          <div class="form-group" style="margin-bottom: 0">
		            <div class="bfh-selectbox bfh-countries" data-country="PH" data-name="establishment[country]" data-flags="true"></div>
		          </div>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		          <button type="submit" class="btn btn-primary">Update</button>
		        </div>
		      </form>
		    </div>
		  </div>
		</div>
		<!-- End of Modal -->
	</body>
</html>