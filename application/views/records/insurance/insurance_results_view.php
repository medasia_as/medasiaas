<!DOCTYPE html>
<html lang="en">
	<head>
		<script>
		$(document).ready(function () {
			$(".selectAll").click(function () {
				var checked_status = this.checked;
				$(".selMulti").each(function () {
					this.checked = checked_status;
				});
			});
			$('#verify-password-form').bootstrapValidator({
				live: 'submitted',
				fields: {
					password: {
						message: 'The username is not valid',
						validators: {
							remote: {
								type: 'POST',
								message: 'Invalid password.',
								url: '<?=base_url()?>login/verify',
								data: {password: $("input[name='password']").val()},
								onSuccess: function(e, data) {
									$( "#verify-password-form" ).submit(function() {
										$.ajax({
											type: "POST",
											url: "<?=base_url()?>records/insurance/updateDeleteInsurance",
											data: $("#delete_insurance_list").serialize(),
											dataType: 'json',
											success: function(data) {
												console.log(data);
												document.location = "<?=base_url()?>" + data.redirect;
											}
										});
										return false;
									});
								}
							},
							notEmpty: {
								message: 'The password is required'
							}
						}
					}
				}
			});
			$('#verifyPassword').on('shown.bs.modal', function() {
				$('#verify-password-form').bootstrapValidator('resetForm', true);
			});
			$( "#verify-password-form" ).submit(function() {
			});
		});
</script>
	</head>
	<body>
		<?php echo validation_errors(); ?>

		<?php echo form_open('', array('id'=>'delete_insurance_list')); ?>

		<?php
			$template = array(
						'table_open'	=> '<table border="0" cellpadding="4" cellspacing="0">',
						'table_close'	=> '</table>'
						);
			$this->table->set_template($template);
			// $multi = $this->table->add_row(form_submit(array('name'=>'submit','value'=>'Delete','class'=>'btn btn-danger')),form_checkbox(array('name'=>'selectAll','id'=>'selectAll', 'class'=>'selectAll')),form_label('Select All'));

			$multi = $this->table->add_row(form_submit(array('name' => 'submit', 'value' => 'Delete', 'class' => 'btn btn-danger','id'=>'delete','data-target'=>'#verifyPassword','data-toggle'=>'modal')),form_checkbox(array('name'=>'selectAll','id'=>'selectAll', 'class'=>'selectAll')),form_label('Select All'));
										   
			echo $this->table->generate($multi);

			echo '<div class="table_scroll">';

			$tmpl = array (
							'table_open'          => '<table border="1" cellpadding="4" cellspacing="0" class="table table-hover table-bordered">',

							'heading_row_start'   => '<tr>',
							'heading_row_end'     => '</tr>',
							'heading_cell_start'  => '<th>',
							'heading_cell_end'    => '</th>',

							'row_start'           => '<tr>',
							'row_end'             => '</tr>',
							'cell_start'          => '<td>',
							'cell_end'            => '</td>',

							'row_alt_start'       => '<tr>',
							'row_alt_end'         => '</tr>',
							'cell_alt_start'      => '<td>',
							'cell_alt_end'        => '</td>',

							'table_close'         => '</table>'
							);

			$this->table->set_template($tmpl);

			$this->table->set_heading('', 'Insurance name', 'Attention Name'.'-'.'Attention Position', 'Address', 'Street', 'City', 'State', 'Postal Code','Status');
			$count=1;
			foreach ($insurance as $value => $key) {
				if($key['status'] == 'DELETED')
	            {
	                $color = 'red';
	            }
	            else
	            {
	                $color = 'black';
	            }
				// Build the custom actions links.
				// $actions = anchor(base_url()."records/insurance/delete/".$key['id']."/", "Delete");
				$selMulti = form_checkbox(array('name'=>'selMulti[]','id'=>'selMulti','class'=>'selMulti','value'=>$key['id']));
				// Adding a new table row.
				$this->table->add_row(
                                            "<font color=".$color.">".$count++.".".$selMulti,
                                            "<font color=".$color.">".anchor(base_url()."records/accounts/view/insurance/".$key['id']."/", $key['name']),
                                            "<font color=".$color.">".$key['attention_name'].' <br />'.$key['attention_position'],
                                            "<font color=".$color.">".$key['address'],
                                            "<font color=".$color.">".$key['street'], /**$key['vendor_account'],**/
                                            "<font color=".$color.">".$key['city'],
                                            "<font color=".$color.">".$key['state'],
                                            "<font color=".$color.">".$key['postal_code'],
                                            "<font color=".$color.">".$key['status']
                                        );
			}
			echo $this->table->generate();
			echo '</div>';
			echo form_hidden('location', 'records/insurance');
			echo form_close();
		?>

















<div class="modal fade" id="verifyPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <form method="post" id="verify-password-form" action="" role="form">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="myModalLabel">Verify Password</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <input type="password" class="form-control" placeholder="Enter Password" name="password" size="20">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger">Verify</button>
        </div>
      </form>
    </div>
  </div>
</div>
	</body>
</html>