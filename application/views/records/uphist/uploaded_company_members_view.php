<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Medriks - Members Results</title>
		<link href="<?php echo base_url();?>bootstrap/css/bootstrap.css" rel="stylesheet">
		<script>
		$(document).ready(function() {
			$(".selectAll").click(function() {
				var checked_status = this.checked;
				$(".selMulti").each(function() {
					this.checked = checked_status;
				});
			});
		$('#verify-password-form').bootstrapValidator({
  live: 'submitted',
  fields: {
    password: {
      message: 'The username is not valid',
      validators: {
        remote: {
          type: 'POST',
          message: 'Invalid password.',
          url: '<?=base_url()?>login/verify',
          data: {password: $("input[name='password']").val()},
          onSuccess: function(e, data) {
            $( "#verify-password-form" ).submit(function() {
              $.ajax({
  				    type: "POST",
  						url: "<?=base_url()?>records/uphist/deleteUploadedRecords",
  						data: $("#delete_members_list").serialize(),
  						dataType: 'json',
  				    success: function(data) {
  			      console.log(data);
              document.location = "<?=base_url()?>" + data.redirect;
  			    	}
  				  });
            return false;
            });
          }
        },
        notEmpty: {
          message: 'The password is required'
        }
      }
   }
  }
});
    $('#verifyPassword').on('shown.bs.modal', function() {
	    $('#verify-password-form').bootstrapValidator('resetForm', true);
			});
    $( "#verify-password-form" ).submit(function() {
		});
		});
		</script>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<?php echo form_open('',array('id'=>'delete_members_list')); ?>
		<?php
		echo "<h4>You uploaded the file <b>".$file_details['filename']."</b> for <b>".$company_insurance["company"]." - ".$company_insurance["insurance"]."</b></h4>";
		echo "<h5>If all details below are correct, click SAVE. If not, you may delete the entire batch by clicking DELETE.</h5>";
		echo "<div id='error_message'></div>";
		if(empty($members_details))
		{
			echo "<h2>No record/s found!!!</h2>";
		}
		else
		{
			$template = array(
			'table_open'	=> '<table border="0" cellpadding="4" cellspacing="0">',
			'table_close'	=> '</table>'
			);
			$this->table->set_template($template);
			$save = anchor(base_url()."records/compins/members/".$company_insurance['id']."/",'Save',array('class'=>'btn btn-success'));
			$multi = $this->table->add_row($save,form_submit(array('name'=>'submit','value'=>'Delete','id'=>'delete','data-toggle'=>"modal",'data-target'=>"#verifyPassword",'class'=>'btn btn-danger')),form_checkbox(array('name'=>'selectAll','id'=>'selectAll', 'class'=>'selectAll')),form_label('Select All'));
			echo $this->table->generate($multi);

			date_default_timezone_set("Asia/Manila");
			$date = date_default_timezone_get();
			echo '<div class="table_scroll">';
			$tmpl = array (
				'table_open'          => '<table border="0" cellpadding="4" cellspacing="0" class="table table-hover table-bordered" id="example">',

				'heading_row_start'   => '<tr>',
				'heading_row_end'     => '</tr>',
				'heading_cell_start'  => '<th>',
				'heading_cell_end'    => '</th>',

				'row_start'           => '<tr>',
				'row_end'             => '</tr>',
				'cell_start'          => '<td>',
				'cell_end'            => '</td>',

					// 'row_alt_start'       => '<tr>',
					// 'row_alt_end'         => '</tr>',
					// 'cell_alt_start'      => '<td>',
					// 'cell_alt_end'        => '</td>',

				'table_close'         => '</table>'
				);
			$this->table->set_template($tmpl);
			$this->table->set_heading('', 'Name','Membership Status', 'Date of Birth', 'Age', 'Level/Position', 'Declaration Date', 'Start', 'End', 'Cardholder Type', 'Cardholder', 'Beneficiary','PhilHealth Benefit','Pre-Existing Condition', 'Remarks');//, 'Benefit Set Name');

			$count=1;
			$currentDate = date('Y-m-d');
			foreach ($members_details as $value => $key)
			{
				if(strtolower($key['status']) == "active")
				{
					$newdate = strtotime('-7 day', strtotime($key['end']));
					$newdate = date('Y-m-d', $newdate);
					$expires = (strtotime($key['end']) - strtotime(date("Y-m-d"))) / (60 * 60 * 24);

					if($expires > 1)
					{
						$day = " days";
					}
					else
					{
						$day = " day";
					}

					if($expires < 0)
					{
						$id = $key['id'];
						$field = 'status';
						$data = "EXPIRED";
						$key['status'] = status_update('patient',$field,$data,$id);
					}

					if($newdate <= $currentDate)
					{ // WARNING
						$color = 'orange';
						$key['status'] = $key['status']." - will expire in ".$expires.$day.".";
					}
					else
					{ // ACTIVE
						$color = 'black';
					}
				}
				elseif (strtolower($key['status']) == "expired" || strtolower($key['status']) == "deleted")
				{ // EXPIRED/DELETED
					$color = 'red';
				}
				else
				{ //ON HOLD OR LACK OF INFO
					$color = 'green';
				}

				// Build the custom actions links.
				// $actions = anchor(base_url()."records/members/delete/".$key['id']."/", "Delete");
				$selMulti = form_checkbox(array('name'=>'selMulti[]','id'=>'selMulti','class'=>'selMulti','value'=>$key['special_id']));
				// Adding a new table row.
				$this->table->add_row("<font color=".$color.">".$count++.".".$selMulti, anchor(base_url()."records/members/view/".$key['id']."/", "<font color=".$color.">".$key['lastname'].", ".$key['firstname']." ".$key['middlename']), "<font color=".$color.">".$key['status'], "<font color=".$color.">".mdate('%M %d, %Y', mysql_to_unix($key['dateofbirth'])), "<font color=".$color.">".computeAge($key['dateofbirth']),
					"<font color=".$color.">".$key['level'], "<font color=".$color.">".mdate('%M %d, %Y', mysql_to_unix($key['declaration_date'])), "<font color=".$color.">".mdate('%M %d, %Y', mysql_to_unix($key['start'])), "<font color=".$color.">".mdate('%M %d, %Y', mysql_to_unix($key['end'])),
					"<font color=".$color.">".$key['cardholder_type'], "<font color=".$color.">".$key['cardholder'], "","<font color=".$color.">".$key['philhealth'],"<font color=".$color.">".$key['pre_existing_condition'], "<font color=".$color.">".$key['remarks']);//, "<font color=".$color."><b>".$key['benefit_name'][0]['benefit_set_name'].'</b><br>Remaining Overall MBL: <b>PHP. '.$key['overall_mbl'].'</b>');
			}
			echo $this->table->generate();
			echo '</div>';
			echo form_hidden('location', 'records/members');
			echo form_hidden('compins_id',$company_insurance['id']);
			}
			echo form_close();
		?>
	</body>

	<div class="modal fade" id="verifyPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <form method="post" id="verify-password-form" action="" role="form">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="myModalLabel">Verify Password</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <input type="password" class="form-control" placeholder="Enter Password" name="password" size="20">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger">Verify</button>
        </div>
      </form>
    </div>
  </div>
</div>


</html>